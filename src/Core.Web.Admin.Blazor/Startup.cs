using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AntDesign.ProLayout;
using Core.Web.Admin.Blazor.Services;
using Core.FrameWork.Commons.ORM.Abstraction;
using Core.Web.Api.Utils.Init;
using Core.FrameWork.Commons.ORM;
using Core.FrameWork.Commons.App;
using Blazored.LocalStorage;
using Core.Web.Admin.Blazor.Utils;
using Core.FrameWork.Commons.ORM.UnitWork;
using Core.Freamwork.Repository;
using Core.FrameWork.Commons.ORM.Repositories;
using Core.Web.Api.Application.SysAdminUser;

namespace Core.Web.Admin.Blazor
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            WebHostEnvironment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment WebHostEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            }).AddCookie("Cookies");
            services.AddControllersWithViews();
            services.AddBlazoredLocalStorage();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddAntDesign();
            services.AddScoped(sp => new HttpClient
            {
                BaseAddress = new Uri(sp.GetService<NavigationManager>().BaseUri)
            });
            services.Configure<ProSettings>(Configuration.GetSection("ProSettings"));
            services.AddScoped<IChartService, ChartService>();
            services.AddScoped<IProjectService, ProjectService>();
            //services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProfileService, ProfileService>();



            //初始化全局配置
            AppCenter.Services = services;
            AppCenter.Configuration = Configuration;
            AppCenter.webHostEnvironment = WebHostEnvironment;
            services.InitUploadFile();//初始化上传文件
            services.AddAutoScanInjection();//自动配置仓储服务注册
            services.AddScoped(typeof(SysAdminUserApp));
            services.AddScoped(typeof(IUnitWork<>), typeof(UnitWork<>));
            services.AddScoped(typeof(IRepository<,>), typeof(BaseRepository<,>));
            //services.TryAddSingleton<IHttpContextUser, HttpContextUser>();
            services.AddScoped<CurrentUserContext, CurrentUserContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            // ******
            // BLAZOR COOKIE Auth Code (begin)
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();
            // BLAZOR COOKIE Auth Code (end)
            // ******

            app.UseEndpoints(endpoints =>
            {
                //支持MVC路由，跳转登录
                endpoints.MapDefaultControllerRoute();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
