namespace Core.Web.Admin.Blazor.Models
{
    public class ActivityProject
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
}