﻿using System;
using System.Collections.Generic;

namespace Core.Web.Admin.Blazor.Models
{
    public class CurrentUserInfo
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string TrueName { get; set; }

        public List<string> Role { get; set; }
    }
}
