﻿namespace Core.Web.Admin.Blazor.Models.System
{
    public class TagType
    {
        public string Key { get; set; }
        public string Label { get; set; }
    }

    public class GeographicType
    {
        public TagType Province { get; set; }
        public TagType City { get; set; }
    }

   

    public class UserLiteItem
    {
        public string Avater { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}