﻿using Core.FrameWork.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Web.Admin.Blazor.Models.System.User
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class AdminUser
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        [Required(ErrorMessage = "账号不能为空！")]
        public string Account { get; set; } = string.Empty;
        /// <summary>
        /// 角色
        /// </summary>
        [Required(ErrorMessage = "角色不能为空！")]
        [DisplayName("角色")]
        public string Role { get; set; } = string.Empty;
        /// <summary>= string.Empty;
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空！")]
        [EmailAddress(ErrorMessage = "邮箱格式错误！")]
        [DisplayName("邮箱")]
        public string Email { get; set; } = string.Empty;
        /// <summary>
        /// 电话
        /// </summary>
        [Required(ErrorMessage = "电话不能为空！")]
        [DisplayName("电话")]
        public string PhoneNumber { get; set; } = string.Empty;
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空！")]
        public string Password { get; set; } = string.Empty;
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Required(ErrorMessage = "真实姓名不能为空！")]
        [DisplayName("真实姓名")]
        public string TrueName { get; set; } = string.Empty;
        /// <summary>
        /// 地址
        /// </summary>
        [DisplayName("地址")]
        public string Address { get; set; } = string.Empty;
        /// <summary>
        /// 公司
        /// </summary>        
        [DisplayName("公司")]
        public string Company { get; set; } = string.Empty;
        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; } = string.Empty;
        ///// <summary>
        ///// 创建时间
        ///// </summary>
        //[DisplayName("创建时间")]
        //public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
