﻿using System.ComponentModel.DataAnnotations;

namespace Core.Web.Admin.Blazor.Models.System.User
{
    /// <summary>
    /// 登录用户入参
    /// </summary>
    public class AdminUserLogin
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required] public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required] public string Password { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [EmailAddress(ErrorMessage = "邮箱格式错误")] public string Emial { get; set; }
        /// <summary>
        /// 邮箱验证码
        /// </summary>
        public string Captcha { get; set; }
        /// <summary>
        /// 登录类型
        /// </summary>
        public string LoginType { get; set; } = "1";//默认使用账号登录
    }

}