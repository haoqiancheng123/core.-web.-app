﻿using Core.Web.Admin.Blazor.Models;
using Core.Web.Admin.Blazor.Services;
using AntDesign.ProLayout;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AntDesign;
using Microsoft.AspNetCore.Components.Authorization;
using Core.Web.Admin.Blazor.Utils;
using Core.Web.Admin.Blazor.Models.System;
using Core.FrameWork.Models;
using Core.FrameWork.Commons.Mapping;
using Core.Web.Admin.Blazor.Models.System.User;
using Microsoft.AspNetCore.Mvc;
using Core.FrameWork.Commons.App;
using Core.Web.Api.Application.SysAdminUser;
using Core.FrameWork.Models.Models.System;

namespace Core.Web.Admin.Blazor.Components
{
    public partial class RightContent
    {
        private AdminUser AdminUserViewModel = new AdminUser();
        [Inject] protected CurrentUserContext GetCurrentUser { get; set; }
        //[Inject] protected ISysAdminUserRepository ISysAdminUserRepository { get; set; }
        private CurrentUserInfo currentUser
        {
            get
            {
                return GetCurrentUser.GetCurrentUser().Result;
            }
        }
        private NoticeIconData[] _notifications = { };
        private NoticeIconData[] _messages = { };
        private NoticeIconData[] _events = { };
        private int _count = 0;

        private List<AutoCompleteDataItem<string>> DefaultOptions { get; set; } = new List<AutoCompleteDataItem<string>>
        {
            new AutoCompleteDataItem<string>
            {
                Label = "umi ui",
                Value = "umi ui"
            },
            new AutoCompleteDataItem<string>
            {
                Label = "Pro Table",
                Value = "Pro Table"
            },
            new AutoCompleteDataItem<string>
            {
                Label = "Pro Layout",
                Value = "Pro Layout"
            }
        };

        public AvatarMenuItem[] AvatarMenuItems { get; set; } = new AvatarMenuItem[]
        {
            new() { Key = "setting", IconType = "setting", Option = "个人设置"},
            new() { IsDivider = true },
            new() { Key = "logout", IconType = "logout", Option = "退出登录"}
        };
        [Inject]protected CurrentUserContext CurrentUserContext { get; set; }
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] protected IProjectService ProjectService { get; set; }
        [Inject] protected MessageService MessageService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                await base.OnInitializedAsync();
                SetClassMap();
                var userId = System.Guid.Parse(currentUser.Id);
                var UserService = (SysAdminUserApp)AppCenter.ServiceProvider.GetService(typeof(SysAdminUserApp));

                AdminUserViewModel =(  UserService.Repository.GetSingleOrDefault(s => s.Id.Equals(userId))).MapTo<SysAdminUser, AdminUser>();


                if (AdminUserViewModel.Avatar == null || AdminUserViewModel.Avatar == "")
                {
                    AdminUserViewModel.Avatar = AdminConfig.AdminSetting.DownloadUrl + "Images/Avatar.png";
                }
                var notices = await ProjectService.GetNoticesAsync();
                _notifications = notices.Where(x => x.Type == "notification").Cast<NoticeIconData>().ToArray();
                _messages = notices.Where(x => x.Type == "message").Cast<NoticeIconData>().ToArray();
                _events = notices.Where(x => x.Type == "event").Cast<NoticeIconData>().ToArray();
                _count = notices.Length;
            }
            catch (System.Exception ex)
            {

                throw;
            }
           
        }

        protected void SetClassMap()
        {
            ClassMapper
                .Clear()
                .Add("right");
        }

        public void HandleSelectUser(MenuItem item)
        {
            switch (item.Key)
            {
                case "center":
                    NavigationManager.NavigateTo("/account/center");
                    break;
                case "setting":
                    NavigationManager.NavigateTo("/account/settings");
                    break;
                case "logout":
                    NavigationManager.NavigateTo("/Account/Logout", true);
                    break;
            }
        }

        public void HandleSelectLang(MenuItem item)
        {
        }

        public async Task HandleClear(string key)
        {
            switch (key)
            {
                case "notification":
                    _notifications = new NoticeIconData[] { };
                    break;
                case "message":
                    _messages = new NoticeIconData[] { };
                    break;
                case "event":
                    _events = new NoticeIconData[] { };
                    break;
            }
            await MessageService.Success($"清空了{key}");
        }

        public async Task HandleViewMore(string key)
        {
            await MessageService.Info("Click on view more");
        }
    }
}