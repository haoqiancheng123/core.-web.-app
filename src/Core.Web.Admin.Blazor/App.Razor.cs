﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;

namespace Core.Web.Admin.Blazor
{
    public partial class App
    {
        [Inject] AuthenticationStateProvider AuthenticationStateProvider { get; set; }
        [Inject] NavigationManager NavigationManager { get; set; }
        //protected override async Task OnitializedAynsc()
        //{
        //    var state = await AuthenticationStateProvider.GetAuthenticationStateAsync();
        //    if (!state.User.Identity.IsAuthenticated)
        //    {
        //        NavigationManager.NavigateTo("Authorization/Index");

        //    }
        //    else
        //    {
        //        await base.OnInitializedAsync();
        //    }
        //}
    }
}
