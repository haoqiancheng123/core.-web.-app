using System.Threading.Tasks;
using Core.Web.Admin.Blazor.Models;
using Core.Web.Admin.Blazor.Services;
using Microsoft.AspNetCore.Components;

namespace Core.Web.Admin.Blazor.Pages.Profile
{
    public partial class Basic
    {
        private BasicProfileDataType _data = new BasicProfileDataType();

        [Inject] protected IProfileService ProfileService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            _data = await ProfileService.GetBasicAsync();
        }
    }
}