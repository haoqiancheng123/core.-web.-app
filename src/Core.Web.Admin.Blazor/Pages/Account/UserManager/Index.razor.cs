﻿using AntDesign;
using AntDesign.TableModels;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Mapping;
using Core.FrameWork.Models.Models.System;
using Core.Web.Admin.Blazor.Models.System.User;
using Core.Web.Api.Application.SysAdminUser;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Admin.Blazor.Pages.Setting.UserManager
{
    public partial class Index
    {
        private readonly Random _random = new Random();
        FrameWork.Commons.Pages.PagerInfo pagerInfo = new FrameWork.Commons.Pages.PagerInfo();
        public IEnumerable<AdminUser> data = new List<AdminUser>();
        ITable table;
        IEnumerable<AdminUser> selectedRows;
        string selectionType = "checkbox";

        string userName = "";
        [Inject] MessageService messageService { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            pagerInfo.CurrenetPageIndex = 1;
            pagerInfo.PageSize = 10;


            var UserService = (SysAdminUserApp)AppCenter.ServiceProvider.GetService(typeof(SysAdminUserApp));

            data = (await UserService.Repository.GetByPaginationAsync(null, pagerInfo, null, null)).MapTo<SysAdminUser, AdminUser>();
        }
        public async Task OnSearch()
        {
            await messageService.Loading($"searching {123}", 2);
        }
        public async Task OnChange(QueryModel<AdminUser> queryModel)
        {
            //var UserService = (IUserService)AppCenter.ServiceProvider.GetService(typeof(IUserService));
            //data = await UserService.GetPageListAsync(null, pagerInfo, null, null);
        }
        public void RemoveSelection(string key)
        {
            var selected = selectedRows.Where(x => x.Account != key).ToList();
            table.SetSelection(selected.Select(x => x.Account).ToArray());
        }
        private async Task Delete(Guid id)
        {
            //var userService = (IUserService)AppCenter.ServiceProvider.GetService(typeof(IUserService));
            //if (await userService.DeleteAsync(id) == 0)
            //{

            //    await messageService.Success("删除失败");
            //}
            //else
            //{
            //    await messageService.Success("删除成功");
        
            //}
            //data = await userService.GetPageListAsync(null, pagerInfo, null, null);
        }
        private void Cancel()
        {
            messageService.Success("Clicked on No");
        }

    }
}
