using System;
using System.Linq;
using System.Threading.Tasks;
using AntDesign;
using Core.FrameWork.Commons.Mapping;
using Core.FrameWork.Models;
using Core.Web.Admin.Blazor.Models;
using Core.Web.Admin.Blazor.Models.System;
using Core.Web.Admin.Blazor.Services;
using Core.Web.Admin.Blazor.Utils;
using Microsoft.AspNetCore.Components;
using System.Text.Json;
using Core.FrameWork.Commons.ApiResult;
using System.Collections.Generic;
using Newtonsoft.Json;
using Core.Web.Admin.Blazor.Models.System.User;
using Core.FrameWork.Commons.ORM.UnitWork;
using Core.Web.Api.Application.SysAdminUser;
using Core.FrameWork.Commons.App;

namespace Core.Web.Admin.Blazor.Pages.Account.Settings
{
    public partial class BaseView
    {
        private readonly Random _random = new Random();
        private AdminUser AdminUser = new AdminUser();
        [Inject] protected CurrentUserContext GetCurrentUser { get; set; }
        [Inject] public MessageService Message { get; set; }//提示
        [Inject] NavigationManager NavigationManager { get; set; }
        private CurrentUserInfo currentUser
        {
            get
            {
                return GetCurrentUser.GetCurrentUser().Result;
            }
        }
        bool loading = false;//加载逻辑字段
        private readonly string UploadUrl = AdminConfig.AdminSetting.DownloadUrl + "Upload/UploadFile";//上传文件路径
        /// <summary>
        /// 上传文件加载后
        /// </summary>
        /// <param name="fileinfo"></param>
        /// <returns></returns>
        public async Task OnSingleCompleted(UploadInfo fileinfo)
        {
            var UserService = (SysAdminUserApp)AppCenter.ServiceProvider.GetService(typeof(SysAdminUserApp));

            loading = true;
            //fileinfo.
            if (fileinfo.File.State == UploadState.Success)
            {
                var response = JsonConvert.DeserializeObject<CommonResult<List<UploadResult>>>(fileinfo.File.Response);
                if (response.Success)
                {
                    AdminUser.Avatar = JsonConvert.DeserializeObject<List<UploadResult>>(JsonConvert.SerializeObject(response.ResData))[0].FileUrl;

                    var userInfo = await UserService.Repository.GetSingleAsync(System.Guid.Parse(currentUser.Id));

                    var updateModel = AdminUser.MapTo(userInfo);

                    UserService.UnitWork.Update(updateModel, nameof(AdminUser.Avatar));

                    var isOk = await UserService.UnitWork.SaveAsync();


                    loading = false;
                    if (isOk == 0)
                        await Message.Error("更新失败");
                    else
                        await Message.Success("更新成功");


                }
                else
                {
                    loading = false;
                    await Message.Error("上传文件接口错误!");
                    AdminUser.Avatar = AdminConfig.AdminSetting.DownloadUrl + "Images/Avatar.png";
                }
            }
            else
            {
                AdminUser.Avatar = AdminConfig.AdminSetting.DownloadUrl + "Images/Avatar.png";
            }
            NavigationManager.NavigateTo("/account/settings", true);
        }
        private async Task UpdateUserInfo()
        {
            try
            {
                loading = true;

                var UserService = (SysAdminUserApp)AppCenter.ServiceProvider.GetService(typeof(SysAdminUserApp));

                var userInfo = await UserService.Repository.GetSingleAsync(System.Guid.Parse(currentUser.Id));

                var updateModel = AdminUser.MapTo(userInfo);

                UserService.UnitWork.Update(updateModel, typeof(AdminUser).GetProperties().Where(s => s.PropertyType != typeof(Guid)).Select(s => s.Name).ToArray());

                var isOk = await UserService.UnitWork.SaveAsync();
                loading = false;
                if (isOk == 0)
                    await Message.Error("更新失败");
                else
                    await Message.Success("更新成功");
            }
            catch (System.Exception ex)
            {

                throw;
            }
            NavigationManager.NavigateTo("/account/settings", true);
        }

        protected override async Task OnInitializedAsync()
        {
            var UserService = (SysAdminUserApp)AppCenter.ServiceProvider.GetService(typeof(SysAdminUserApp));

            var userInfo = await UserService.Repository.GetSingleAsync(System.Guid.Parse(currentUser.Id));

            AdminUser = userInfo.MapTo<Core.FrameWork.Models.Models.System.SysAdminUser, AdminUser>();

            if (AdminUser.Avatar == null || AdminUser.Avatar == "")
            {
                AdminUser.Avatar = AdminConfig.AdminSetting.DownloadUrl + "Images/Avatar.png";
            }

            await base.OnInitializedAsync();



        }
    }
}