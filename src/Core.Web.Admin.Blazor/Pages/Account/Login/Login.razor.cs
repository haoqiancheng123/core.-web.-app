﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using AntDesign;
using Core.FrameWork.Commons.Email.Core;
using System;
using Core.Web.Admin.Blazor.Models.System.User;
using Core.Web.Api.Application.SysAdminUser;

namespace Core.Web.Admin.Blazor.Pages.User
{
    public partial class Login
    {
        bool loading = false;
        private readonly AdminUserLogin _model = new AdminUserLogin();
        private readonly Random _random = new Random();

        [Inject] public NavigationManager NavigationManager { get; set; }//跳转
        [Inject] public SysAdminUserApp SysAdminUserRepository { get; set; }//用户仓储

         

        [Inject] public MessageService Message { get; set; }//提示
        /// <summary>
        /// 登录用户
        /// </summary>
        public async Task HandleSubmit()
        {
            FrameWork.Commons.Pages.PagerInfo pagerInfo = new FrameWork.Commons.Pages.PagerInfo();
            var aa=  await SysAdminUserRepository.Repository.GetByPaginationAsync(null, pagerInfo, null);


            string url = "/Account/Login?userId={0}&userName={1}&trueName={2}&role={3}&returnUrl={4}";
            if (_model.LoginType == "2")//邮箱登录
            {
                if (string.IsNullOrEmpty(_model.Captcha))
                {
                    loading = false;
                    await Message.Error("验证码不能为空！");
                    return;
                }
                var accountInfo = await SysAdminUserRepository.Repository.GetSingleOrDefaultAsync(s => s.Email.Equals(_model.Emial));
                if (accountInfo == null)
                {
                    loading = false;
                    await Message.Error("账号不存在！");
                    return;
                }

            }
            else if (_model.LoginType == "1")
            {
                var accountInfo = await SysAdminUserRepository.Repository.GetSingleOrDefaultAsync(s => s.Account.Equals(_model.UserName) || s.PhoneNumber.Equals(_model.UserName));
                if (accountInfo == null)
                {
                    loading = false;
                    await Message.Error("账号不存在！");
                    return;
                }
                else
                {
                    if (accountInfo.Password.Trim().Equals(_model.Password))
                    {
                        loading = false;
                        NavigationManager.NavigateTo(string.Format(url, accountInfo.Id, accountInfo.Account, accountInfo.TrueName, accountInfo.Role, ""), true);
                        return;
                    }
                    else
                    {
                        loading = false;
                        await Message.Error("密码错误！");
                    }
                }
            }
        }
        /// <summary>
        /// 发送邮箱验证码
        /// </summary>
        /// <returns></returns>
        public async Task GetEmailCode()
        {
            loading = true;
            if (string.IsNullOrEmpty(_model.Emial))
            {
                loading = false;
                await Message.Error("错误");
            }
            SendMailHelper sendMailHelper = new SendMailHelper();
            if (await sendMailHelper.SendEmailAsync(new MailBodyEntity()
            {
                Recipients = new System.Collections.Generic.List<string>() { _model.Emial },
                Subject = "登录验证码",
                Body = _random.Next(0, 9999).ToString().PadLeft(4, '0')
            }))
            {
                loading = false;
                await Message.Success($"发送成功!");
            }

            else
            {
                loading = false;
                await Message.Error($"发送失败!");
            }
        }
        public async Task OnTabChange(string key)
        {
            _model.LoginType = key;
            await Task.CompletedTask;
        }
    }
}