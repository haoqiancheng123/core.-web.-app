﻿using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Extensions;
using Core.FrameWork.Commons.ORM.DataManager;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Core.FrameWork.Commons
{
    /// <summary>
    /// 配置文件读取操作
    /// </summary>
    public class Configs
    {

        /// <summary>
        /// 根据Key读取并赋值给对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Bind<T>(string key, T obj)
        {
            GetSection(key).Bind(obj);
            return obj;
        }

        /// <summary>
        /// 根据Key Value读取并赋值给对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Bind<T>( string key,string setion, T obj)
        {
            GetSection(key).GetSection(setion).Bind(obj);
            return obj;
        }

        /// <summary>
        /// 根据Key获取数配置内容
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IConfigurationSection GetSection(string key)
        {
            return AppCenter.Configuration.GetSection(key);
        }
        /// <summary>
        /// 根据section和key获取配置内容
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigurationValue(string section, string key)
        {
            return GetSection(section)[key] ?? "";
        }

        /// <summary>
        /// 根据Key获取数据库连接字符串
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            return AppCenter.Configuration.GetConnectionString(key);
        }
        static Configs()
        {
            DbConnectConfig = new DbConnectConfigs();
            DbConnections = new DbConnections();
            CacheProvider = new CacheProviders();
            EmailConfig = new EmailConfig();
            BlobConfig = new Blob();
            AppSetting = new AppSetting();
        }

        public static AppSetting AppSetting { get; set; }
        public static DbConnectConfigs DbConnectConfig { get; set; }
        public static DbConnections DbConnections { get; set; }
        public static CacheProviders CacheProvider { get; set; }
        public static EmailConfig EmailConfig { get; set; }
        public static Blob BlobConfig { get; set; }
    }
    #region 实体
    /// <summary>
    /// 全局配置
    /// </summary>
    public class AppSetting
    {
        /// <summary>
        /// 下载路径
        /// </summary>
        public string DownloadUrl => AppCenter.Configuration.GetSection("AppSetting")["DownloadUrl"];
        /// <summary>
        /// 是否使用存储
        /// </summary>
        public bool IsBlob => AppCenter.Configuration.GetSection("AppSetting")["IsBlob"].ToBool();
    }
    #region 数据库配置 ----------------------
    /// <summary>
    /// 数据库连接配置
    /// </summary>
    public class DbConnections
    {

        public DbConnectionOptions DictWrite => Configs.GetSection("DbConnections:" + Configs.DbConnectConfig.DefaultDataBase + ":MasterDB").Get<DbConnectionOptions>();

        public Dictionary<string, DbConnectionOptions> DictRead => Configs.GetSection("DbConnections:" + Configs.DbConnectConfig.DefaultDataBase + ":ReadDb").Get<Dictionary<string, DbConnectionOptions>>();
    }
    #endregion
    #region 数据库配置-----------------------
    /// <summary>
    /// 数据库配置
    /// </summary>
    public class DbConnectConfigs
    {
        /// <summary>
        /// 数据库连接字符串是否加密
        /// </summary>
        public string ConStringEncrypt => AppCenter.Configuration.GetSection("DbConnectConfig")["ConStringEncrypt"];
        /// <summary>
        /// 默认使用数据库
        /// </summary>
        public string DefaultDataBase => AppCenter.Configuration.GetSection("DbConnectConfig")["DefaultDataBase"];
        /// <summary>
        /// 开启数据库读写分离
        /// </summary>
        public string IsDBReadWriteSeparate => AppCenter.Configuration.GetSection("DbConnectConfig")["IsDBReadWriteSeparate"];
        /// <summary>
        /// //定义多个从数据库的访问策略
        /// </summary>
        public string QueryDBStrategy => AppCenter.Configuration.GetSection("DbConnectConfig")["QueryDBStrategy"];
    }
    #endregion
    #region 缓存配置   -----------------------
    /// <summary>
    /// 缓存中间件
    /// </summary>
    public class CacheProviders
    {
        /// <summary>
        /// 是否使用Redis
        /// </summary>
        public bool UseRedis => AppCenter.Configuration.GetSection("CacheProvider")["UseRedis"].ToBool();
        public string Redis_ConnectionString => AppCenter.Configuration.GetSection("CacheProvider")["Redis_ConnectionString"];
        public string Redis_InstanceName => AppCenter.Configuration.GetSection("CacheProvider")["Redis_InstanceName"];
        public string Cache_Memcached_Configuration => AppCenter.Configuration.GetSection("CacheProvider")["Cache_Memcached_Configuration"];
    }
    #endregion
    #region SMTP服务器
    public class EmailConfig
    {
        /// <summary>
        /// SMTP服务器
        /// </summary>
        public string MailHost => AppCenter.Configuration.GetSection("EmailConfig")["MailHost"];
        /// <summary>
        /// 邮箱账号
        /// </summary>
        public string MailAccount => AppCenter.Configuration.GetSection("EmailConfig")["MailAccount"];
        /// <summary>
        /// 邮箱密码
        /// </summary>
        public string Emailpassword => AppCenter.Configuration.GetSection("EmailConfig")["Emailpassword"];
        /// <summary>
        /// SSL加密连接
        /// </summary>
        public string MailPwd => AppCenter.Configuration.GetSection("EmailConfig")["MailPwd"];
        /// <summary>
        /// SMTP端口
        /// </summary>
        public string MailPort => AppCenter.Configuration.GetSection("EmailConfig")["MailPort"];
        /// <summary>
        /// 是否开启SSL
        /// </summary>
        public bool MailSSLEnabled => AppCenter.Configuration.GetSection("EmailConfig")["MailSSLEnabled"].ToBool();

        /// <summary>
        /// 模板地址
        /// </summary>
        public string EmailTemplateFolder => AppCenter.Configuration.GetSection("EmailConfig")["EmailTemplateFolder"];

        /// <summary>
        /// 模板地址
        /// </summary>
        public string MailSendName => AppCenter.Configuration.GetSection("EmailConfig")["MailSendName"];


    }


    #endregion
    #region AzureBlob 存储配置 ---------------------
    /// <summary>
    /// AzureBlob 配置
    /// </summary>
    public class Blob
    {
        /// <summary>
        /// URL
        /// </summary>
        public virtual string UrlBase => AppCenter.Configuration.GetSection("Blob")["UrlBase"];
        /// <summary>
        /// 协议
        /// </summary>
        public virtual string Protocol => AppCenter.Configuration.GetSection("Blob")["Protocol"];
        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account => AppCenter.Configuration.GetSection("Blob")["Account"];
        /// <summary>
        /// KEY
        /// </summary>
        public virtual string Key => AppCenter.Configuration.GetSection("Blob")["Key"];
        /// <summary>
        /// 终结点
        /// </summary>
        public virtual string EndPoint => AppCenter.Configuration.GetSection("Blob")["EndPoint"];

        public override string ToString()
        {
            return string.Format("DefaultEndpointsProtocol={0};AccountName={1};AccountKey={2};BlobEndpoint={3}", Protocol, Account, Key, EndPoint);
        }

    }
    #endregion
}
#endregion


