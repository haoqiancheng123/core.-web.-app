﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.ORM.Abstraction
{
    /// <summary>
    /// Mysql DbContext接口
    /// </summary>
    public interface IMySqlDbContext: IDbContextCore
    {
    }
}
