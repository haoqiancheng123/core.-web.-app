﻿using Core.FrameWork.Commons.ORM.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.ORM.Abstraction
{
    public interface IDbContextFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writeAndRead">指定读、写操作</param>
        /// <returns></returns>
        DbContext CreateContext(WriteAndReadEnum writeAndRead);
        /// <summary>
        /// 创建数据库读写上下文
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="writeAndRead">指定读、写操作</param>
        /// <returns></returns>
        DbContext CreateContext<TEntity>(WriteAndReadEnum writeAndRead);
    }
}
