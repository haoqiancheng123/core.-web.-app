﻿using Core.FrameWork.Commons.DependencyInjection;
using Core.FrameWork.Commons.ORM.Abstraction;
using Core.FrameWork.Commons.ORM.DataManager;
using Core.FrameWork.Commons.ORM.Enums;
using Core.FrameWork.Commons.ORM.Models;
using Core.FrameWork.Commons.Pages;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Core.FrameWork.Commons.ORM.Repositories
{
    /// <summary>
    /// 泛型仓储，实现泛型仓储接口
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <typeparam name="TKey">实体主键类型</typeparam>
    public  class BaseRepository<T, TDbContext> : IRepository<T, TDbContext>, IScopedDependency
        where T : BaseEntity where TDbContext : DbContext
    {
        #region EF构造函数及基本配置
        private TDbContext _dbContext;
        /// <summary>
        /// 
        /// </summary>
        protected DbSet<T> DbSet => DbContext.Set<T>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public BaseRepository()
        {
            _dbContext= (TDbContext)DbContextFactory.CreateContext<TDbContext>(WriteAndReadEnum.Read);
        }

        #endregion
        #region Dapper构造函数及基本配置
        /// <summary>
        /// 获取访问数据库配置
        /// </summary>
        protected DbConnectionOptions dbConnectionOptions = DBServerProvider.GeDbConnectionOptions<T>();
        /// <summary>
        /// 需要初始化的对象表名
        /// </summary>
        protected string tableName = typeof(T).GetCustomAttribute<System.ComponentModel.DataAnnotations.Schema.TableAttribute>(false)?.Name;
        /// <summary>
        /// 数据库参数化访问的占位符
        /// </summary>
        protected string parameterPrefix = "@";
        /// <summary>
        /// 防止和保留字、关键字同名的字段格式，如[value]
        /// </summary>
        protected string safeFieldFormat = "[{0}]";
        /// <summary>
        /// 数据库的主键字段名,若主键不是Id请重载BaseRepository设置
        /// </summary>
        protected string primaryKey = "Id";
        /// <summary>
        /// 排序字段
        /// </summary>
        protected string sortField;
        /// <summary>
        /// 是否为降序
        /// </summary>
        protected bool isDescending = true;
        /// <summary>
        /// 选择的字段，默认为所有(*) 
        /// </summary>
        protected string selectedFields = " * ";
        /// <summary>
        /// 是否开启多租户
        /// </summary>
        protected bool isMultiTenant = false;


        /// <summary>
        /// 排序字段
        /// </summary>
        public string SortField
        {
            get
            {
                return sortField;
            }
            set
            {
                sortField = value;
            }
        }

        /// <summary>
        /// 数据库访问对象的外键约束
        /// </summary>
        public string PrimaryKey
        {
            get
            {
                return primaryKey;
            }
        }


        #endregion


        #region EF操作
        public DatabaseFacade GetDatabase()
        {
            return _dbContext.Database;
        }
        /// <summary>
        /// EF 上下文接口，可读可写
        /// </summary>
        public virtual TDbContext DbContext
        {
            get { return _dbContext; }
        }

    

        #region Query

        public IQueryable<T> Find(Expression<Func<T, bool>> @where = null)
        {
            return DbContext.Set<T>().AsNoTracking().Where(@where);
        }

        /// <summary>
        /// 根据条件统计数量Count() 
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual int Count(Expression<Func<T, bool>> @where = null)
        {
            return Find(@where).Count();
        }

        /// <summary>
        /// 根据条件统计数量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> @where = null)
        {
            return await Find(@where).CountAsync(where);
        }
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>

        public virtual bool Exist(Expression<Func<T, bool>> @where = null)
        {
            return Find(@where).Any();
        }
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<bool> ExistAsync(Expression<Func<T, bool>> @where = null)
        {
            return await Find(where).AnyAsync();
        }

        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual T GetSingleOrDefault(Expression<Func<T, bool>> @where)
        {
            return Find(@where).FirstOrDefault();
        }

        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> @where)
        {
            return await Find(@where).FirstOrDefaultAsync();
        }
        public T GetSingle(Guid key)
        {
            return  Find(s=>s.Id.Equals(key)).FirstOrDefault();
        }

        public async Task<T> GetSingleAsync(Guid key)
        {
            return await Find(s => s.Id.Equals(key)).FirstOrDefaultAsync();
        }

        public T GetSingleOrDefault(Expression<Func<T, bool>> where, bool[] asc, params Expression<Func<T, object>>[] orderby)
        {
            return Find(where).FirstOrDefault();
        }

        public async Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> where, bool[] asc, params Expression<Func<T, object>>[] orderby)
        {
            return await Find(where).FirstOrDefaultAsync();
        }
        /// <summary>
        /// 获取实体列表(排序)。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IList<T> Get(Expression<Func<T, bool>> @where)
        {
            return Find(@where).ToList();
        }
        public virtual async Task<IList<T>> GetAsync(Expression<Func<T, bool>> @where)
        {
            return await Find(@where).ToListAsync();
        }

        /// <summary>
        /// 获取实体列表(排序)。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IList<T> Get(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby)
        {
            var filter = Find(where);
            if (orderby != null)
            {
                for (int i = 0; i < @orderby.Length; i++)
                {
                    if (i == 0)
                    {
                        filter = asc[i] ? filter.OrderBy(@orderby[i]) : filter.OrderByDescending(@orderby[i]);
                    }
                    else
                        filter = asc[i] ? ((IOrderedQueryable<T>)filter).ThenBy(@orderby[i]) : ((IOrderedQueryable<T>)filter).ThenByDescending(@orderby[i]);

                }
            }
            return filter.ToList();
        }

        public virtual async Task<IList<T>> GetAsync(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby)
        {
            var filter = Find(where);
            if (orderby != null)
            {
                for (int i = 0; i < @orderby.Length; i++)
                {
                    if (i == 0)
                    {
                        filter = asc[i] ? filter.OrderBy(@orderby[i]) : filter.OrderByDescending(@orderby[i]);
                    }
                    else
                        filter = asc[i] ? ((IOrderedQueryable<T>)filter).ThenBy(@orderby[i]) : ((IOrderedQueryable<T>)filter).ThenByDescending(@orderby[i]);

                }
            }
            return await filter.ToListAsync();
        }
        /// <summary>
        ///  分页获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="pagerInfo">分页信息</param>
        /// <param name="asc">排序方式</param>
        /// <param name="orderby">排序字段</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetByPagination(Expression<Func<T, bool>> @where, PagerInfo pagerInfo, bool[] asc, params Expression<Func<T, object>>[] @orderby)
        {
            var filter = Find(where);
            if (orderby != null)
            {
                for (int i = 0; i < @orderby.Length; i++)
                {
                    if (i == 0)
                    {
                        filter = asc[i] ? filter.OrderBy(@orderby[i]) : filter.OrderByDescending(@orderby[i]);
                    }
                    else
                        filter = asc[i] ? ((IOrderedQueryable<T>)filter).ThenBy(@orderby[i]) : ((IOrderedQueryable<T>)filter).ThenByDescending(@orderby[i]);

                }
            }
            pagerInfo.RecordCount = filter.Count();
            return filter.Skip(pagerInfo.PageSize * (pagerInfo.CurrenetPageIndex - 1)).Take(pagerInfo.PageSize);
        }
        public virtual async Task<IList<T>> GetByPaginationAsync(Expression<Func<T, bool>> @where, PagerInfo pagerInfo, bool[] asc,
    params Expression<Func<T, object>>[] @orderby)
        {
            var filter = Find(where);
            if (orderby != null)
            {
                for (int i = 0; i < @orderby.Length; i++)
                {
                    if (i == 0)
                    {
                        filter = asc[i] ? filter.OrderBy(@orderby[i]) : filter.OrderByDescending(@orderby[i]);
                    }
                    else
                        filter = asc[i] ? ((IOrderedQueryable<T>)filter).ThenBy(@orderby[i]) : ((IOrderedQueryable<T>)filter).ThenByDescending(@orderby[i]);

                }
            }
            pagerInfo.RecordCount = await filter.CountAsync();
            return await filter.Skip(pagerInfo.PageSize * (pagerInfo.CurrenetPageIndex - 1)).Take(pagerInfo.PageSize).ToListAsync();
        }
        /// <summary>
        /// sql语句查询数据集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<T> GetBySql(string sql)
        {
            return DbContext.Set<T>().FromSqlRaw(sql).ToList();
        }
        /// <summary>
        /// sql语句查询数据集，返回输出Dto实体
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<TView> GetViews<TView>(string sql)
        {
            return DbContext.Set<T>().FromSqlRaw(sql).Cast<TView>().ToList();
        }
        /// <summary>
        /// 查询视图
        /// </summary>
        /// <typeparam name="TView">返回结果对象</typeparam>
        /// <param name="viewName">视图名称</param>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public List<TView> GetViews<TView>(string viewName, Func<TView, bool> @where)
        {
            return DbContext.Set<T>().FromSqlRaw($"select * from {viewName}").Cast<TView>().ToList();
        }

        #endregion
        #endregion

        #region Dapper 操作


        /// <summary>
        /// 用Dapper原生方法操作数据，支持读写操作
        /// </summary>
        public IDbConnection DapperConn
        {
            get { return new DapperDbContext().GetConnection<T>(); }
        }

        /// <summary>
        /// 用Dapper原生方法，仅用于只读数据库
        /// </summary>
        public IDbConnection DapperConnRead
        {
            get { return new DapperDbContext().GetConnection<T>(false); }
        }

        #region 查询获得对象和列表
        /// <summary>
        /// 根据id获取一个对象
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <returns></returns>
        public virtual T DapperGet(Guid primaryKey)
        {
            return DapperConnRead.Get<T>(primaryKey);
        }
        /// <summary>
        /// 异步根据id获取一个对象
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <returns></returns>
        public virtual async Task<T> DapperGetAsync(Guid primaryKey)
        {
            return await DapperConnRead.GetAsync<T>(primaryKey);
        }
        /// <summary>
        /// 根据id 获取一个对象，未删除正常数据
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public virtual T DapperGetIsNotDeleteAndEnabledMark(Guid primaryKey)
        {
            string sql = $"select * from { tableName} ";
            sql += " where IsDelete=0 and Enabled=1";
            return DapperConnRead.QueryFirstOrDefault<T>(sql);
        }
        /// <summary>
        /// 根据条件获取一个对象
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public virtual T DapperGetWhere(string where)
        {
            if (HasInjectionData(where))
            {
                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"select * from { tableName} ";
            sql += " where " + where;
            return DapperConnRead.QueryFirstOrDefault<T>(sql);
        }

        /// <summary>
        /// 根据条件异步获取一个对象
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        public virtual async Task<T> DapperGetWhereAsync(string where)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"select * from { tableName} ";
            sql += " where " + where;

            return await DapperConnRead.QueryFirstOrDefaultAsync<T>(sql);
        }

        /// <summary>
        /// 获取所有数据，谨慎使用
        /// </summary>
        /// <param name="trans">事务</param>
        /// <returns></returns>
        public virtual IEnumerable<T> DapperGetAll(IDbTransaction trans = null)
        {
            return DapperGetListWhere();
        }
        /// <summary>
        /// 获取所有数据，谨慎使用
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> DapperGetAllAsync(IDbTransaction trans = null)
        {
            return await DapperGetListWhereAsync();
        }


        /// <summary>
        /// 根据查询条件获取数据集合
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual IEnumerable<T> DapperGetListWhere(string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            string sql = $"select {selectedFields} from { tableName} ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where;
            }
            return DapperConnRead.Query<T>(sql, trans);
        }

        /// <summary>
        /// 根据查询条件异步获取数据集合
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> DapperGetListWhereAsync(string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            string sql = $"select {selectedFields}  from { tableName} ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where;
            }
            return await DapperConnRead.QueryAsync<T>(sql, trans);
        }

        /// <summary>
        /// 根据查询条件查询前多少条数据
        /// </summary>
        /// <param name="top">多少条数据</param>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual IEnumerable<T> DapperGetListTopWhere(int top, string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }

            string sql = $"select top {top} {selectedFields} from " + tableName; ;
            if (dbConnectionOptions.DatabaseType == DatabaseType.SqlServer)
            {
                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
            }
            else if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select {selectedFields} from " + tableName;

                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
                sql += $"  LIMIT 0,{top}; ";
            }
            return DapperConnRead.Query<T>(sql, trans);
        }


        /// <summary>
        /// 根据查询条件异步查询前多少条数据
        /// </summary>
        /// <param name="top">多少条数据</param>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> DapperGetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            string sql = $"select top {top} {selectedFields} from " + tableName;
            if (dbConnectionOptions.DatabaseType == DatabaseType.SqlServer)
            {
                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
            }
            else if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select {selectedFields} from " + tableName;

                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
                sql += $"  LIMIT 0,{top}; ";
            }
            return await DapperConnRead.QueryAsync<T>(sql, trans);
        }
        /// <summary>
        /// 查询软删除的数据，如果查询条件为空，即查询所有软删除的数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual IEnumerable<T> DapperGetAllByIsIsDelete(string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            string sqlWhere = " IsDelete=1 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return DapperGetListWhere(sqlWhere, trans);
        }

        /// <summary>
        /// 查询未软删除的数据，如果查询条件为空，即查询所有未软删除的数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual IEnumerable<T> DapperGetAllByIsNotIsDelete(string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            string sqlWhere = " IsDelete=0 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return DapperGetListWhere(sqlWhere, trans);
        }


        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual List<T> DapperFindWithPager(string condition, PagerInfo info, IDbTransaction trans = null)
        {
            return DapperFindWithPager(condition, info, this.SortField, this.isDescending, trans);
        }

        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual List<T> DapperFindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return DapperFindWithPager(condition, info, fieldToSort, this.isDescending, trans);
        }

        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual async Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return await DapperFindWithPagerAsync(condition, info, fieldToSort, this.isDescending, trans);
        }

        /// <summary>
        /// 根据条件异步查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual async Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null)
        {
            return await DapperFindWithPagerAsync(condition, info, this.SortField, trans);
        }
        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual List<T> DapperFindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            List<T> list = new List<T>();

            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            PagerHelper pagerHelper = new PagerHelper(this.tableName, this.selectedFields, fieldToSort, info.PageSize, info.CurrenetPageIndex, desc, condition);

            string pageSql = pagerHelper.GetPagingSql(true, dbConnectionOptions.DatabaseType);
            pageSql += ";" + pagerHelper.GetPagingSql(false, dbConnectionOptions.DatabaseType);

            var reader = DapperConnRead.QueryMultiple(pageSql);
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        public virtual async Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {

            List<T> list = new List<T>();

            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }

            PagerHelper pagerHelper = new PagerHelper(this.tableName, this.selectedFields, fieldToSort, info.PageSize, info.CurrenetPageIndex, desc, condition);

            string pageSql = pagerHelper.GetPagingSql(true, dbConnectionOptions.DatabaseType);
            pageSql += ";" + pagerHelper.GetPagingSql(false, dbConnectionOptions.DatabaseType);

            var reader = await DapperConnRead.QueryMultipleAsync(pageSql);
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 分页查询，自行封装sql语句(仅支持sql server)
        /// 非常复杂的查询，可在具体业务模块重写该方法
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="info">分页信息</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual List<T> DapperFindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            List<T> list = new List<T>();
            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始记录
            int endNum = info.CurrenetPageIndex * info.PageSize;//结束记录
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select {0} FROM {1} where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by {0}) AS rows ,{1} FROM {2} where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);
            var reader = DapperConnRead.QueryMultiple(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 分页查询，自行封装sql语句(仅支持sql server)
        /// 非常复杂的查询，可在具体业务模块重写该方法
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="info">分页信息</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> DapperFindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始记录
            int endNum = info.CurrenetPageIndex * info.PageSize;//结束记录
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select {0} FROM {1} where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by {0}) AS rows ,{1} FROM {2} where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);
            var reader = await DapperConnRead.QueryMultipleAsync(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            List<T> list = reader.Read<T>().AsList();
            return list;
        }
        /// <summary>
        /// 根据条件统计数据
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="fieldName">统计字段名称</param>
        /// <returns></returns>
        public virtual int DapperGetCountByWhere(string condition, string fieldName = "*")
        {
            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            string sql = $"select count({fieldName}) from {tableName}  where ";
            if (!string.IsNullOrWhiteSpace(condition))
            {
                sql = sql + condition;
            }
            return DapperConnRead.Query<int>(sql).FirstOrDefault();
        }

        /// <summary>
        /// 根据条件异步统计数据
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="fieldName">统计字段名称</param>
        /// <returns></returns>
        public virtual async Task<int> DapperGetCountByWhereAsync(string condition, string fieldName = "*")
        {
            if (HasInjectionData(condition))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            string sql = $"select count({fieldName}) from {tableName}  where ";
            if (!string.IsNullOrWhiteSpace(condition))
            {
                sql = sql + condition;
            }
            return await DapperConnRead.QueryFirstAsync<int>(sql);
        }

        /// <summary>
        /// 根据条件查询获取某个字段的最大值
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="where">条件</param>
        /// <param name="trans">事务</param>
        /// <returns>返回字段的最大值</returns>
        public virtual async Task<decimal> DapperGetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            string sql = $"select isnull(MAX({strField}),0) as maxVaule from {tableName} ";
            if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select if(isnull(MAX({strField})),0,MAX({strField})) as maxVaule from {tableName} ";
            }
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }

            return await DapperConnRead.QueryFirstAsync<decimal>(sql);
        }
        /// <summary>
        /// 根据条件统计某个字段之和,sum(字段)
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="where">条件</param>
        /// <param name="trans">事务</param>
        /// <returns>返回字段求和后的值</returns>
        public virtual async Task<decimal> DapperGetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            string sql = $"select isnull(sum({strField}),0) as sumVaule from {tableName} ";
            if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select if(isnull(sum({strField})),0,sum({strField})) as sumVaule from {tableName} ";
            }
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            return await DapperConnRead.QueryFirstAsync<decimal>(sql);
        }
        #endregion

        #region 新增、修改和删除

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual long DapperInsert(T entity, IDbTransaction trans = null)
        {
            if (entity.Id!=Guid.Empty)
            {
                entity.Id=Guid.NewGuid();
            }
            return DapperConn.Insert<T>(entity);
        }


        /// <summary>
        /// 异步新增
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual async Task<long> DapperInsertAsync(T entity, IDbTransaction trans = null)
        {
            if (entity.Id != Guid.Empty)
            {
                entity.Id = Guid.NewGuid();
            }
            return await DapperConn.InsertAsync<T>(entity);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperUpdateAsync(T entity, Guid primaryKey, IDbTransaction trans = null)
        {
            return await DapperConn.UpdateAsync<T>(entity);
        }


        /// <summary>
        /// 物理删除信息
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperDelete(Guid primaryKey, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + "=@PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });

            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 异步物理删除信息
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperDeleteAsync(Guid primaryKey, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + "=@PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 按主键批量删除
        /// </summary>
        /// <param name="ids">主键Id List集合</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperDeleteBatch(IList<dynamic> ids, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where Id in (@PrimaryKey)";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = ids });

            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 按条件批量删除
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperDeleteBatchWhere(string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + where;
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);

            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 按条件批量删除
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperDeleteBatchWhereAsync(string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + where;
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 根据指定对象的ID和用户ID,从数据库中删除指定对象(用于记录人员的操作日志）
        /// </summary>
        /// <param name="primaryKey">指定对象的ID</param>
        /// <param name="userId">用户ID</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperDeleteByUser(Guid primaryKey, string userId, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + " = @PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 异步根据指定对象的ID和用户ID,从数据库中删除指定对象(用于记录人员的操作日志）
        /// </summary>
        /// <param name="primaryKey">指定对象的ID</param>
        /// <param name="userId">用户ID</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperDeleteByUserAsync(Guid primaryKey, string userId, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + " = @PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 逻辑删除信息，bl为true时将IsDelete设置为1删除，bl为flase时将IsDelete设置为10-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperDeleteSoft(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "IsDelete=0 ";
            }
            else
            {
                sql += "IsDelete=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + PrimaryKey + "=@PrimaryKey";
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 异步逻辑删除信息，bl为true时将IsDelete设置为1删除，bl为flase时将IsDelete设置为0-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperDeleteSoftAsync(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "IsDelete=0 ";
            }
            else
            {
                sql += "IsDelete=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + PrimaryKey + "=@PrimaryKey";
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 异步批量软删除信息，将IsDelete设置为1-删除，0-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param> c
        /// <param name="where">条件</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual async Task<bool> DapperDeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "IsDelete=0 ";
            }
            else
            {
                sql += "IsDelete=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 设置数据有效性，将EnabledMark设置为1-有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperSetEnabledMark(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "Enabled=1 ";
            }
            else
            {
                sql += "Enabled=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@lastModifyTime where " + PrimaryKey + "=@PrimaryKey";

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @lastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 异步设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperSetEnabledMarkAsync(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "Enabled=1 ";
            }
            else
            {
                sql += "Enabled=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime where " + PrimaryKey + "=@PrimaryKey";

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = PrimaryKey, @LastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 异步按条件设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="where">条件</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        public virtual async Task<bool> DapperSetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "Enabled=1 ";
            }
            else
            {
                sql += "Enabled=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { LastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 异步按条件设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="where">条件</param>
        /// <param name="paramparameters"></param>
        /// <param name="userId"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<bool> DapperSetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "Enabled=1 ";
            }
            else
            {
                sql += "Enabled=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime  " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { LastModifyTime = lastModifyTime, paramparameters });
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 更新某一字段值,字段值字符类型
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值字符类型</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperUpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "='" + fieldValue + "'";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 更新某一字段值,字段值字符类型
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值字符类型</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperUpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "='" + fieldValue + "'";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 更新某一字段值，字段值为数字
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值数字</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual bool DapperUpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "=" + fieldValue + "";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = DapperExecuteTransaction(param);
            return result.Item1;
        }


        /// <summary>
        /// 异步更新某一字段值，字段值为数字
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值数字</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        public virtual async Task<bool> DapperUpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {

                throw new Exception("检测出SQL注入的恶意数据");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "=" + fieldValue + "";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await DapperExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 多表多数据操作批量插入、更新、删除--事务
        /// </summary>
        /// <param name="trans">事务</param>
        /// <param name="commandTimeout">超时</param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> DapperExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            if (!trans.Any()) return new Tuple<bool, string>(false, "执行事务SQL语句不能为空！");
            using (IDbConnection connection = DapperConn)
            {
                bool isClosed = connection.State == ConnectionState.Closed;
                if (isClosed) connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var tran in trans)
                        {
                            await connection.ExecuteAsync(tran.Item1, tran.Item2, transaction, commandTimeout);
                        }
                        //提交事务
                        transaction.Commit();
                        return new Tuple<bool, string>(true, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                    }
                }
            }
        }


        /// <summary>
        /// 多表多数据操作批量插入、更新、删除--事务
        /// </summary>
        /// <param name="trans">事务</param>
        /// <param name="commandTimeout">超时</param>
        /// <returns></returns>
        public Tuple<bool, string> DapperExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            if (!trans.Any()) return new Tuple<bool, string>(false, "执行事务SQL语句不能为空！");
            using (IDbConnection connection = DapperConn)
            {
                bool isClosed = connection.State == ConnectionState.Closed;
                if (isClosed) connection.Open();
                //开启事务
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var tran in trans)
                        {
                            connection.Execute(tran.Item1, tran.Item2, transaction, commandTimeout);
                        }
                        //提交事务
                        transaction.Commit();
                        return new Tuple<bool, string>(true, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                        return new Tuple<bool, string>(false, ex.ToString());
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                    }
                }
            }
        }

        #endregion

        #endregion
        #region 辅助类方法
        /// <summary>
        /// 验证是否存在注入代码(条件语句）
        /// </summary>
        /// <param name="inputData"></param>
        public virtual bool HasInjectionData(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
                return false;

            //里面定义恶意字符集合
            //验证inputData是否包含恶意集合
            if (Regex.IsMatch(inputData.ToLower(), GetRegexString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取正则表达式
        /// </summary>
        /// <returns></returns>
        private static string GetRegexString()
        {
            //构造SQL的注入关键字符
            string[] strBadChar =
            {
                "select\\s",
                "from\\s",
                "insert\\s",
                "delete\\s",
                "update\\s",
                "drop\\s",
                "truncate\\s",
                "exec\\s",
                "count\\(",
                "declare\\s",
                "asc\\(",
                "mid\\(",
                //"char\\(",
                "net user",
                "xp_cmdshell",
                "/add\\s",
                "exec master.dbo.xp_cmdshell",
                "net localgroup administrators"
            };

            //构造正则表达式
            string str_Regex = ".*(";
            for (int i = 0; i < strBadChar.Length - 1; i++)
            {
                str_Regex += strBadChar[i] + "|";
            }
            str_Regex += strBadChar[^1] + ").*";

            return str_Regex;
        }

        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> where = null)
        {
            throw new NotImplementedException();
        }

     

        #endregion

    }
}
