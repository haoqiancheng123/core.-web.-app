﻿using Core.FrameWork.Commons.ORM.Models;
using Core.FrameWork.Commons.Pages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace Core.FrameWork.Commons.ORM.Repositories
{
    /// <summary>
    /// 定义泛型接口,实体仓储模型的数据标准操作
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <typeparam name="TKey">主键类型</typeparam>
    public interface IRepository<T, TDbContext> where T : BaseEntity where TDbContext : DbContext
    {


        #region EF 操作
        public DatabaseFacade GetDatabase();

   

        #region Query 查询
        /// <summary>
        /// 获取IQueryable 对象（用于linq 联查）
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 根据条件统计数量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        int Count(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 根据条件统计数量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> CountAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        bool Exist(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<bool> ExistAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        ///  根据主键获取实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetSingle(Guid key);
        /// <summary>
        ///  根据主键获取实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<T> GetSingleAsync(Guid key);
        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        T GetSingleOrDefault(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        T GetSingleOrDefault(Expression<Func<T, bool>> @where);

        /// <summary>
        /// 获取单个实体。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> @where);
        /// <summary>
        /// 获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IList<T> Get(Expression<Func<T, bool>> @where);

        /// <summary>
        /// 获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<IList<T>> GetAsync(Expression<Func<T, bool>> @where);
        /// <summary>
        /// 获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IList<T> Get(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby);

        /// <summary>
        /// 获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<IList<T>> GetAsync(Expression<Func<T, bool>> @where, bool[] asc, params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        ///  分页获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="pagerInfo">分页信息</param>
        /// <param name="asc">排序方式</param>
        /// <param name="orderby">排序字段</param>
        /// <returns></returns>
        IEnumerable<T> GetByPagination(Expression<Func<T, bool>> @where, PagerInfo pagerInfo, bool[] asc,
            params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        ///  分页获取实体列表。建议：如需使用Include和ThenInclude请重载此方法。
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="pagerInfo">分页信息</param>
        /// <param name="asc">排序方式</param>
        /// <param name="orderby">排序字段</param>
        /// <returns></returns>
        Task<IList<T>> GetByPaginationAsync(Expression<Func<T, bool>> @where, PagerInfo pagerInfo, bool[] asc,
            params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        /// sql语句查询数据集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<T> GetBySql(string sql);
        /// <summary>
        /// sql语句查询数据集，返回输出Dto实体
        /// </summary>
        /// <typeparam name="TView">返回结果对象</typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<TView> GetViews<TView>(string sql);
        /// <summary>
        /// 查询视图
        /// </summary>
        /// <typeparam name="TView">返回结果对象</typeparam>
        /// <param name="viewName">视图名称</param>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<TView> GetViews<TView>(string viewName, Func<TView, bool> where);

        #endregion
        #endregion
        #region dapper 操作

        #region 新增
        /// <summary>
        /// 同步新增实体。
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        long DapperInsert(T entity, IDbTransaction trans = null);

        /// <summary>
        /// 异步新增实体。
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<long> DapperInsertAsync(T entity, IDbTransaction trans = null);



        /// <summary>
        /// 同步物理删除实体。
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        bool DapperDelete(Guid primaryKey, IDbTransaction trans = null);

        /// <summary>
        /// 异步物理删除实体。
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperDeleteAsync(Guid primaryKey, IDbTransaction trans = null);

        /// <summary>
        /// 按主键批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        bool DapperDeleteBatch(IList<dynamic> ids, IDbTransaction trans = null);

        /// <summary>
        /// 按条件批量删除
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        bool DapperDeleteBatchWhere(string where, IDbTransaction trans = null);
        /// <summary>
        /// 异步按条件批量删除
        /// </summary>
        /// <param name="where">条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperDeleteBatchWhereAsync(string where, IDbTransaction trans = null);

        #endregion

        #region 更新操作

        #region 更新实体或批量更新
        /// <summary>
        /// 异步更新实体。
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperUpdateAsync(T entity, Guid primaryKey, IDbTransaction trans = null);
        #endregion

        #region 更新某一字段值
        /// <summary>
        /// 更新某一字段值
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        bool DapperUpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null);

        /// <summary>
        /// 异步更新某一字段值
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<bool> DapperUpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一字段值，字段值为数字
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值数字</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        bool DapperUpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一字段值，字段值为数字
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="fieldValue">字段值数字</param>
        /// <param name="where">条件,为空更新所有内容</param>
        /// <param name="trans">事务对象</param>
        /// <returns>执行成功返回<c>true</c>，否则为<c>false</c>。</returns>
        Task<bool> DapperUpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null);
        #endregion

        #region 逻辑删除
        /// <summary>
        /// 同步软删除信息，将IsDelete设置为1-删除，0-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        bool DapperDeleteSoft(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 异步软删除信息，将IsDelete设置为1-删除，0-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param> c
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperDeleteSoftAsync(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 异步批量软删除信息，将IsDelete设置为1-删除，0-恢复删除
        /// </summary>
        /// <param name="bl">true为不删除，false删除</param> c
        /// <param name="where">条件</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperDeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);

        #endregion

        #region 数据有效性
        /// <summary>
        /// 设置数据有效性，将EnabledMark设置为1-有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        bool DapperSetEnabledMark(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 异步设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="primaryKey">主键ID</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperSetEnabledMarkAsync(bool bl, Guid primaryKey, string userId = null, IDbTransaction trans = null);


        /// <summary>
        /// 异步按条件设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="where">条件</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperSetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);
        /// <summary>
        /// 异步按条件设置数据有效性，将EnabledMark设置为1:有效，0-为无效
        /// </summary>
        /// <param name="bl">true为有效，false无效</param>
        /// <param name="where">条件</param>
        /// <param name="paramparameters">参数</param>
        /// <param name="userId">操作用户</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<bool> DapperSetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null);

        #endregion

        #endregion

        #region 查询


        #region 单个实体
        /// <summary>
        /// 同步查询单个实体。
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <returns></returns>
        T DapperGet(Guid primaryKey);

        /// <summary>
        /// 异步查询单个实体。
        /// </summary>
        /// <param name="primaryKey">主键</param>
        /// <returns></returns>
        Task<T> DapperGetAsync(Guid primaryKey);

        /// <summary>
        /// 同步查询单个实体。
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        T DapperGetWhere(string where);

        /// <summary>
        /// 异步查询单个实体。
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        Task<T> DapperGetWhereAsync(string where);

        #endregion

        /// <summary>
        /// 获取所有数据，谨慎使用
        /// </summary>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        IEnumerable<T> DapperGetAll(IDbTransaction trans = null);

        /// <summary>
        /// 获取所有数据，谨慎使用
        /// </summary>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<IEnumerable<T>> DapperGetAllAsync(IDbTransaction trans = null);


        /// <summary>
        /// 根据查询条件查询数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        IEnumerable<T> DapperGetListWhere(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 异步根据查询条件查询数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<IEnumerable<T>> DapperGetListWhereAsync(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根据查询条件查询前多少条数据
        /// </summary>
        /// <param name="top">多少条数据</param>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        IEnumerable<T> DapperGetListTopWhere(int top, string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根据查询条件查询前多少条数据
        /// </summary>
        /// <param name="top">多少条数据</param>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        Task<IEnumerable<T>> DapperGetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 查询软删除的数据，如果查询条件为空，即查询所有软删除的数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="trans">事务对象</param>
        /// <returns></returns>
        IEnumerable<T> DapperGetAllByIsIsDelete(string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 查询未软删除的数据，如果查询条件为空，即查询所有未软删除的数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <param name="tran">事务对象</param>
        /// <returns></returns>
        IEnumerable<T> DapperGetAllByIsNotIsDelete(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        List<T> DapperFindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        List<T> DapperFindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        Task<List<T>> DapperFindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null);

        /// <summary>
        /// 根据条件查询数据库,并返回对象集合(用于分页数据显示)
        /// </summary>
        /// <param name="condition">查询的条件</param>
        /// <param name="info">分页实体</param>
        /// <param name="trans">事务对象</param>
        /// <returns>指定对象的集合</returns>
        List<T> DapperFindWithPager(string condition, PagerInfo info, IDbTransaction trans = null);


        /// <summary>
        /// 分页查询，自行封装sql语句(仅支持sql server)
        /// 非常复杂的查询，可在具体业务模块重写该方法
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="info">分页信息</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<List<T>> DapperFindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 分页查询，自行封装sql语句(仅支持sql server)
        /// 非常复杂的查询，可在具体业务模块重写该方法
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="info">分页信息</param>
        /// <param name="fieldToSort">排序字段</param>
        /// <param name="desc">排序方式 true为desc，false为asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        List<T> DapperFindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根据条件统计数据
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="fieldName">统计字段名称</param>
        /// <returns></returns>
        int DapperGetCountByWhere(string condition, string fieldName = "*");
        /// <summary>
        /// 根据条件统计数据
        /// </summary>
        /// <param name="condition">查询条件</param>
        /// <param name="fieldName">统计字段名称</param>
        /// <returns></returns>
        Task<int> DapperGetCountByWhereAsync(string condition, string fieldName = "*");

        /// <summary>
        /// 根据条件查询获取某个字段的最大值
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="where">条件</param>
        /// <param name="trans">事务</param>
        /// <returns>返回字段的最大值</returns>
        Task<decimal> DapperGetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        /// <summary>
        /// 根据条件统计某个字段之和,sum(字段)
        /// </summary>
        /// <param name="strField">字段</param>
        /// <param name="where">条件</param>
        /// <param name="trans">事务</param>
        /// <returns>返回字段求和后的值</returns>
        Task<decimal> DapperGetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        #endregion

        #region 多表批量操作，支持事务

        /// <summary>
        /// 多表操作--事务
        /// </summary>
        /// <param name="trans">事务</param>
        /// <param name="commandTimeout">超时</param>
        /// <returns></returns>
        Task<Tuple<bool, string>> DapperExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null);
        /// <summary>
        /// 多表操作--事务
        /// </summary>
        /// <param name="trans">事务</param>
        /// <param name="commandTimeout">超时</param>
        /// <returns></returns>
        Tuple<bool, string> DapperExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null);

        #endregion

        #endregion
    }
}
