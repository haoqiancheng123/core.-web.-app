﻿using Core.FrameWork.Commons.ORM.Abstraction;
using Core.FrameWork.Commons.ORM.DataManager;
using Core.FrameWork.Commons.ORM.Enums;
using Core.Freamwork.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.ORM
{
    public class DbContextFactory
        
        //: IDbContextFactory
    {
        /// <summary>
        /// 创建数据库读写上下文
        /// </summary>
        /// <param name="writeAndRead">指定读、写操作</param>
        /// <returns></returns>
        public static DbContext CreateContext(WriteAndReadEnum writeAndRead)
        {
            DbConnectionOptions dbConnectionOptions = new DbConnectionOptions();
            switch (writeAndRead)
            {
                case WriteAndReadEnum.Write:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions(true);
                    break;
                case WriteAndReadEnum.Read:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions(false);
                    break;
                default:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions(true);
                    break;
            }
            return new BaseDbContext(dbConnectionOptions);
        }


        /// <summary>
        /// 创建数据库读写上下文
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="writeAndRead">指定读、写操作</param>
        /// <returns></returns>
        public static DbContext CreateContext<TEntity>(WriteAndReadEnum writeAndRead)
        {
            DbConnectionOptions dbConnectionOptions = new DbConnectionOptions();
            switch (writeAndRead)
            {
                case WriteAndReadEnum.Write:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions<TEntity>(true);
                    break;
                case WriteAndReadEnum.Read:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions<TEntity>(false);
                    break;
                default:
                    dbConnectionOptions = DBServerProvider.GeDbConnectionOptions<TEntity>(true);
                    break;
            }
            return new BaseDbContext(dbConnectionOptions);
        }
    }
}
