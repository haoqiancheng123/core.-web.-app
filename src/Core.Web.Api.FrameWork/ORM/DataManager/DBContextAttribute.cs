﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.ORM.DataManager
{
    public class DBContextAttribute: Attribute
    {
        /// <summary>
        /// 数据库配置名称
        /// </summary>
        public string DbConfigName { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dbConfigName"></param>
        public DBContextAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }
}
