﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Loging.Model
{
    public class ReqEfLogInfo
    {
        public string Sql { get; set; }

        public DateTime DateTime { get; set; }
    }
}
