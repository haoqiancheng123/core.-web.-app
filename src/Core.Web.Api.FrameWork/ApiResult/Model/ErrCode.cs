﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.ApiResult.Model
{
    /// <summary>
    /// 错误代码描述
    /// </summary>
    public static class ErrCode
    {

        /// <summary>
        /// 请求成功
        /// </summary>
        public static string successMsg = "请求成功";
        public static string successCode = "200";

        /// <summary>
        /// 请求失败
        /// </summary>
        public static string errorMsg = "请求失败";
        public static string errorCode = "-100";


        /// <summary>
        /// 获取access_token时AppID或AppSecret错误。请开发者认真比对appid和AppSecret的正确性，或查看是否正在为恰当的应用调用接口
        /// </summary>
        public static string err40001Msg = "获取access_token时AppID或AppSecret错误。请开发者认真比对appid和AppSecret的正确性，或查看是否正在为恰当的应用调用接口";
        public static string err40001 = "err40001";


        /// <summary>
        /// 调用接口的服务器URL地址不正确，请联系供应商进行设置
        /// </summary>
        public static string err40002Msg = "调用接口的服务器URL地址不正确，请联系供应商进行授权";
        public static string err40002 = "err40002";

        /// <summary>
        /// 请确保grant_type字段值为client_credential
        /// </summary>
        public static string err40003Msg = "请确保grant_type字段值为client_credential";
        public static string err40003 = "err40003";

        /// <summary>
        /// 不合法的凭证类型
        /// </summary>
        public static string err40004Msg = "不合法的凭证类型";
        public static string err40004 = "err40004";

        /// <summary>
        /// 用户令牌accesstoken超时失效
        /// </summary>
        public static string err40005Msg = "用户令牌accesstoken超时失效";
        public static string err40005 = "err40005Msg";

        /// <summary>
        /// 您未被授权使用该功能，请重新登录试试或联系管理员进行处理
        /// </summary>
        public static string err40006Msg = "您未被授权使用该功能，请重新登录试试或联系系统管理员进行处理";
        public static string err40006 = "err40006";

        /// <summary>
        /// 传递参数出现错误
        /// </summary>
        public static string err40007Msg = "传递参数出现错误";
        public static string err40007 = "err40007";

        /// <summary>
        /// 用户未登录或超时
        /// </summary>
        public static string err40008Msg = "用户未登录或超时";
        public static string err40008 = "err40008";
        /// <summary>
        /// 程序异常
        /// </summary>
        public static string err40110Msg = "程序异常";
        public static string err40110 = "err40110Msg";

        /// <summary>
        /// 更新数据失败
        /// </summary>
        public static string err43001Msg = "新增数据失败";
        public static string err43001 = "err43001";

        /// <summary>
        /// 更新数据失败
        /// </summary>
        public static string err43002Msg = "更新数据失败";
        public static string err43002 = "err43002";

        /// <summary>
        /// 物理删除数据失败
        /// </summary>
        public static string err43003Msg = "删除数据失败";
        public static string err43003 = "err43003";

        /// <summary>
        /// 该用户不存在
        /// </summary>
        public static string err50001Msg = "该用户不存在";
        public static string err50001 = "err50001";

        /// <summary>
        /// 该用户已存在
        /// </summary>
        public static string err50002Msg = "用户已存在，请登录或重新注册！";
        public static string err50002 = "err50002";

        /// <summary>
        /// 会员注册失败
        /// </summary>
        public static string err50003Msg = "会员注册失败";
        public static string err50003 = "err50003";

        /// <summary>
        /// 查询数据不存在
        /// </summary>
        public static string err60001Msg = "查询数据不存在";
        public static string err60001 = "err60001";
    }
}
