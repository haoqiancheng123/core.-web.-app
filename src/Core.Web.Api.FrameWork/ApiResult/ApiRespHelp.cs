﻿

using ARC.Common.Help;
using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Commons.ApiResult.Model;
using System.Collections.Generic;


namespace ARC.Common.Help
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApiRespHelp
    {


        static ApiRespHelp()
        {

        }


        public static CommonResult GetApiDataList<T>(List<T> t)
        {
            CommonResult result = new CommonResult();
            result.ResData = t;
            result.Success = true;
            return result;
        }


        public static CommonResult GetApiDataList<T>(string code = "200", string msg = "请求成功", List<T> t = null)
        {
            CommonResult result = new CommonResult();
            result.Code = code;
            result.Msg = msg;
            result.ResData = t;
            result.Success = true;
            return result;
        }



        public static CommonResult GetApiData<T>(T t)
        {
            CommonResult result = new CommonResult();
            result.ResData = t;
            result.Success = true;
            return result;
        }



        public static CommonResult GetApiData<T>(string code = "200", string msg = "请求成功", T t = null) where T : class
        {
            CommonResult result = new CommonResult();
            result.ResData = t;
            result.Code = code;
            result.Msg = msg;
            result.Success = true;
            return result;
        }

        public static PageResult<T> getApiDataListByPage<T>(List<T> list, int recordCount, int pageIndex, int pageSize)
        {
            PageResult<T> result = new PageResult<T>();
            result.ResData = list;
            result.pageCount = (recordCount % pageSize == 0 ? (recordCount / pageSize) : (recordCount / pageSize) + 1);
            result.recordCount = recordCount;
            result.pageIndex = pageIndex;
            result.pageSize = pageSize;
            result.Success = true;
            return result;
        }
        public static PageResult<T> getApiDataListByPage<T>(object list, int recordCount, int pageIndex, int pageSize)
        {
            PageResult<T> result = new PageResult<T>();
            result.ResData = list;
            result.pageCount = (recordCount % pageSize == 0 ? (recordCount / pageSize) : (recordCount / pageSize) + 1);
            result.recordCount = recordCount;
            result.pageIndex = pageIndex;
            result.pageSize = pageSize;
            result.Success = true;
            return result;
        }

        public static CommonResult getSuccess(string msg = "请求成功")
        {
            CommonResult result = new CommonResult();
            result.Msg = msg;
            result.Success = true;
            return result;
        }
        public static CommonResult getError(string code = "-100", string msg = "请求失败")
        {
            CommonResult result = new CommonResult();
            result.Code = code;
            result.Msg = msg;
            result.Success = true;
            return result;
        }

        public static CommonResult getError<T>(string code = "100", string msg = "请求失败")
        {
            CommonResult result = new CommonResult();
            result.Code = code;
            result.Msg = msg;
            result.Success = false;
            return result;
        }
    }
}
