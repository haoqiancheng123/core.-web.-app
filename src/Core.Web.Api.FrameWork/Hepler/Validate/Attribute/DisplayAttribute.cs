﻿using System;


namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// Url地址效验验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayAttribute : System.Attribute
    {
        public string DisplayName;
        public DisplayAttribute(string _displayName)
        {
            DisplayName = _displayName;
        }

    }
}
