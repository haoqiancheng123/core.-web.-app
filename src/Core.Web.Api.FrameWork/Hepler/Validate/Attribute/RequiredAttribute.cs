﻿using Core.FrameWork.Commons.Hepler.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// 必填验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredAttribute : AbstractValidateAttribute
    {
        public override ValidateResult Validate(object oValue)
        {

            bool flag= oValue != null
                && !string.IsNullOrWhiteSpace(oValue.ToString());
            if (!flag)
            {
                result.ListErrorMessage.Add("是必填字段");
            }
            return result;
        }
    }
}
