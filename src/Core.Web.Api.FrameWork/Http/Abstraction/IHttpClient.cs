﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Http.Abstraction
{
    public interface IHttpClient
    {
        public Task<T> GetAsync<T>(string serviceName, string url);

        public Task<T> PostAsync<T>(string url, HttpContent httpContent);
    }
}
