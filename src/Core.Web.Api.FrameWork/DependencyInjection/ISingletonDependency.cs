﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.FrameWork.Commons.DependencyInjection
{
    /// <summary>
    /// 单例服务注册依赖
    /// </summary>
    public interface ISingletonDependency: IPrivateDependency
    {
    }
}
