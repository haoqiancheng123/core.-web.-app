﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.FrameWork.Commons.DependencyInjection
{

    /// <summary>
    /// 暂时服务注册依赖
    /// </summary>
    public interface ITransientDependency:IPrivateDependency
    {
    }
}
