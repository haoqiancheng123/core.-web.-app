﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.App.HttpContextUser
{
    /// <summary>
    /// 登录人信息
    /// </summary>
    public class AspNetUser
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 用户角色
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// 用户Token
        /// </summary>
        public string Token { get; set; }
    }
}
