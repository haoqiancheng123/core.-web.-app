﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.App.HttpContextUser
{
    public interface IHttpContextUser
    {
        /// <summary>
        /// 获取Token 解析后的Claim 数据
        /// </summary>
        /// <returns></returns>
        public List<string> GetUserInfoFromToken(string ClaimType);
        /// <summary>
        /// 获取用户数据
        /// </summary>
        /// <returns></returns>
        public AspNetUser GetUserInfo();
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        string GetToken();

        IEnumerable<Claim> GetClaimsIdentity(string ClaimType);

        bool IsAuthenticated();
    }

}
