﻿using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Hepler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons
{
    public class AzureConfig
    {
        public static AzureBlobConfig AzureBlobConfig { get; private set; }
        public static BlobConfig BlobConfig { get; private set; }
        public static Dictionary<BlobFileType, string> BlobContainers { get; private set; }


        static AzureConfig()
        {
            AzureBlobConfig azureBlobConfig = new AzureBlobConfig();
            AzureBlobConfig = Configs.Bind("blob", azureBlobConfig);
            AzureBlobConfig = azureBlobConfig;

            BlobConfig blobconfig = new BlobConfig();
            blobconfig = Configs.Bind("blob", "dbsBlob", blobconfig);
            BlobConfig = blobconfig;

            //文件类型
            BlobContainers = new Dictionary<BlobFileType, string>
            {
             {BlobFileType.Face,blobconfig.FaceContainer}
            ,{BlobFileType.Voice,blobconfig.AudioContainer}
             ,{BlobFileType.Image,blobconfig.ImagesContainer}
             ,{BlobFileType.Video,blobconfig.VideoContainer}
             ,{BlobFileType.File,blobconfig.FileContainer}
             ,{ BlobFileType.QrCode,blobconfig.QRCodeContainer}
             ,{ BlobFileType.Poster,blobconfig.PosterContainer}
             ,{BlobFileType.IncreasePoster,blobconfig.IncreasePoster}
             ,{BlobFileType.WxHeadImg,blobconfig.FaceContainer}
            };
        }


    }
    public abstract class ConfigObject { }

    public class AzureBlobConfig : ConfigObject
    {
        public string Protocol { get; set; }
        public string Account { get; set; }
        public string Key { get; set; }
        public string EndPoint { get; set; }

        public AzureBlobConfig()
        {
            Protocol = "https";
        }

        public override string ToString()
        {
            return string.Format("DefaultEndpointsProtocol={0};AccountName={1};AccountKey={2};BlobEndpoint={3}", Protocol, Account, Key, EndPoint);
        }

        public string UrlBase { get; set; }
    }
    public class BlobConfig : ConfigObject
    {
        public string FaceContainer { get; set; }
        public string AudioContainer { get; set; }
        public string ImagesContainer { get; set; }
        public string VideoContainer { get; set; }
        public string FileContainer { get; set; }
        public string QRCodeContainer { get; set; }
        public string PosterContainer { get; set; }
        public string IncreasePoster { get; set; }
    }

}
