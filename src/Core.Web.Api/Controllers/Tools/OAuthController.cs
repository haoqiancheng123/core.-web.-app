﻿using ARC.Common.Help;
using Core.FrameWork.CollectiveOAuth.Utils;
using Core.FrameWork.Commons.ApiResult;
using Core.Web.Api.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Api.Controllers.Tools
{
    /// <summary>
    /// 第三方授权
    /// </summary>
    public class OAuthController : BaseController
    {
        /// <summary>
        /// 构建授权Url方法
        /// </summary>
        /// <param name="authSource">0:微信公众平台 1:微信开放平台 2:企业微信自动授权 3:企业微信扫码 4:支付宝服务窗 5:码云授权 6:Github授权 7:百度开放平台 8:小米开放平台 9:钉钉扫码 10:OSChina开源中国 11:Coding扣钉 12:LinkedIn领英 13:微博 14:腾讯QQ 15:抖音 16:Google(谷歌) 17: Facebook 18:微软 </param>
        /// <returns></returns>
        [HttpGet]
        public CommonResult Authorization(string authSource)
        {
            AuthRequestFactory authRequest = new AuthRequestFactory();

            var request = authRequest.getRequest(authSource);

            var authorize = request.authorize(AuthStateUtils.createState());

            return ApiRespHelp.GetApiData(authorize);
        }
    }
}
