﻿using ARC.Common.Help;
using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Commons.Email.Core;
using Core.FrameWork.Commons.Extensions;
using Core.FrameWork.Commons.Loging;
using Core.FrameWork.Models.Req.Email;
using Core.Web.Api.Application.SysAdminUser;
using Core.Web.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Api.Controllers.Tools
{
    /// <summary>
    /// 工具控制器
    /// </summary>
    public class ToolsController : BaseController
    {
        private readonly SysAdminUserApp _sysAdminUserApp;
        public ToolsController(SysAdminUserApp sysAdminUserApp)
        {
            _sysAdminUserApp = sysAdminUserApp;
        }
        [HttpGet]
        public CommonResult GetEf()
        {
            try
            {
                var A = _sysAdminUserApp.Repository.Get(s => s.Id != null).ToList();
                return ApiRespHelp.GetApiDataList(A);
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 获取省-市-县(如果不全请替换项目city.json文件)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public CommonResult GetArea() => ApiRespHelp.GetApiData(CityHelper.GetCity);
        /// <summary>
        /// 测试邮箱
        /// </summary>
        /// <param name="reqSendEmail">邮件表单</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonResult> TestEmail([FromBody] ReqSendEmail reqSendEmail)
        {
            var reqResult = reqSendEmail.Validate();
            if (!reqResult.Status)
                return ApiRespHelp.getError("-100", reqResult.Message);

            MailBodyEntity mailBodyEntity = new MailBodyEntity()
            {
                Body = reqSendEmail.Body,
                Subject = reqSendEmail.Subject,
                Recipients = reqSendEmail.Recipients,
                Cc = reqSendEmail.Cc,
                Bcc = reqSendEmail.Bcc
            };
            SendMailHelper sendMailHelper = new SendMailHelper();
            await sendMailHelper.SendEmail(mailBodyEntity);
            return ApiRespHelp.getSuccess();
        }
        /// <summary>
        /// 获取日志
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public CommonResult GetLogging(DateTime dateTime) => ApiRespHelp.GetApiDataList(LogLockHelper.GetLogData(dateTime));

    }
}
