using Core.FrameWork.Commons;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.App.HttpContextUser;
using Core.FrameWork.Commons.ORM;
using Core.FrameWork.Commons.ORM.Abstraction;
using Core.FrameWork.Commons.ORM.Repositories;
using Core.FrameWork.Commons.ORM.UnitWork;
using Core.FrameWork.Models;
using Core.FrameWork.Models.Models.System;
using Core.Freamwork.Repository;
using Core.Web.Api.Application.SysAdminUser;
using Core.Web.Api.Middleware;
using Core.Web.Api.Utils.Init;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace Core.Web.Api
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        /// <summary>
        /// 容器注入
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddControllers();
            //初始化全局配置
            AppCenter.Services = services;
            AppCenter.Configuration = Configuration;
            AppCenter.webHostEnvironment=Environment;

            services.UseSwaggerChinese();//Swagger中文文档
            services.UseCache();//缓存中间件
            services.UseJWTAuthentication();//JWT验证
            //services.AddAutoScanInjection();//自动配置仓储服务注册
            services.InitUploadFile();//配置上传文件
            services.AddCorsSetup(); //跨域配置
            services.InitHttpClientPolly();//增加HttpClientFactory-Polly机制
            services.AddScoped(typeof(SysAdminUserApp));
            services.AddScoped(typeof(IUnitWork<>),typeof(UnitWork<>));
            services.AddScoped(typeof(IRepository<,>), typeof(BaseRepository<,>));
            services.TryAddSingleton<IHttpContextUser, HttpContextUser>();

        }

        /// <summary>
        /// 管道
        /// </summary>
        /// <param name="app"></param>
        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            #region 开启Swagger
            //是否开始Swagger
            if (APIConfig.ApiSetting.UseSwagger)
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Core.Web.Api v1");  //注入汉化文件
                    c.InjectJavascript($"/swagger/swagger_translator.js");
                    c.InjectStylesheet($"/swagger/Swagger.css");
                });
            }
            #endregion
            app.UseCors(APIConfig.ApiSetting.CorsPolicyName);//跨域配置

            app.UseMiddleware<IPLogHandlerMilddleware>();//配置IP记录
            app.UseMiddleware<RecordHandlerAccessLogsMilddleware>();//配置用户操作记录
            app.UseMiddleware<ExceptionHandlerMilddleware>();//全局异常配置

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
