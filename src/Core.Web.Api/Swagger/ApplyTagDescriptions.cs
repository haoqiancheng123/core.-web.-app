﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Api.Swagger
{
    public class ApplyTagDescriptions : IDocumentFilter
    {
        /// <summary>
        /// swagger汉化标签
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new List<OpenApiTag>
            {
                new OpenApiTag { Name = "Tools", Description = "工具控制器" },
                new OpenApiTag { Name = "Upload", Description = "文件上传控制器" },
                new OpenApiTag { Name = "OAuth", Description = "第三方授权控制器" },
            };
        }
    }
}
