﻿using Core.FrameWork.Commons.Cache;
using Core.FrameWork.Models;
using Core.Web.Api.Application.SysBaseTree;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Web.Api.Application.Managers
{
    /// <summary>
    /// BaseTree /配置中心
    /// </summary>
    public class ViewBaseTreeManage
    {
        private readonly ICacheService _cacheService;//缓存中间件
        private readonly SysBaseTreeApp _sysBaseTreeRepository;
        public ViewBaseTreeManage(ICacheService cacheService, SysBaseTreeApp sysBaseTreeRepository)
        {
            _cacheService = cacheService;
            _sysBaseTreeRepository = sysBaseTreeRepository;
        }

        #region 业务配置



        #endregion

        /// <summary>
        /// 获取BaseTree Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Core.FrameWork.Models.Models.System.SysBaseTree> GetTreeById(Guid id = default(Guid))
        {
            if (_cacheService.Exists(id.ToString()))//判断缓存中是否有数据
                return _cacheService.Get<Core.FrameWork.Models.Models.System.SysBaseTree>(id.ToString());

            var value =await _sysBaseTreeRepository.Repository.GetSingleAsync(id);

            _cacheService.Add(id.ToString(), value, TimeSpan.FromSeconds(APIConfig.ApiSetting.BaseTreeCacheExpiration));//添加至缓存

            return value;
        }
        /// <summary>
        /// 获取BaseTree Pid 数据
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Core.FrameWork.Models.Models.System.SysBaseTree>> GetBaseTreePid(int pid = 0)
        {

            if (_cacheService.Exists(pid.ToString()))//判断缓存中是否有数据
                return _cacheService.Get<IEnumerable<Core.FrameWork.Models.Models.System.SysBaseTree>>(pid.ToString());

            var list = await _sysBaseTreeRepository.Repository.DapperGetListWhereAsync($" pid={pid}");


            _cacheService.Add(pid.ToString(), list, TimeSpan.FromSeconds(APIConfig.ApiSetting.BaseTreeCacheExpiration));//添加至缓存

            return list;

        }

    }
}
