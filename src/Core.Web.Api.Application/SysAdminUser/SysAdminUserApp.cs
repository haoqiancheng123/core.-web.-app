﻿using Core.Freamwork.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Web.Api.Application.SysAdminUser
{
    public class SysAdminUserApp : BaseApp<Core.FrameWork.Models.Models.System.SysAdminUser, BaseDbContext>
    {
        public SysAdminUserApp(FrameWork.Commons.ORM.UnitWork.IUnitWork<BaseDbContext> unitWork, FrameWork.Commons.ORM.Repositories.IRepository<FrameWork.Models.Models.System.SysAdminUser, BaseDbContext> repository) : base(unitWork, repository)
        {
        }
    }
}
