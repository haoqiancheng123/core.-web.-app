﻿using Core.FrameWork.Commons.ORM.Repositories;
using Core.FrameWork.Commons.ORM.UnitWork;
using Core.FrameWork.Models.Models.System;
using Core.Freamwork.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Web.Api.Application.SysLogs
{
    public class SysLogsApp : BaseApp<SysNLogRecords, BaseDbContext>
    {
        public SysLogsApp(IUnitWork<BaseDbContext> unitWork, IRepository<SysNLogRecords, BaseDbContext> repository) : base(unitWork, repository)
        {
        }
    }
}
