﻿using Core.FrameWork.Commons.ORM.Repositories;
using Core.FrameWork.Commons.ORM.UnitWork;
using Core.Freamwork.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Web.Api.Application.SysBaseTree
{
    public class SysBaseTreeApp : BaseApp<Core.FrameWork.Models.Models.System.SysBaseTree, BaseDbContext>
    {
        public SysBaseTreeApp(IUnitWork<BaseDbContext> unitWork, IRepository<FrameWork.Models.Models.System.SysBaseTree, BaseDbContext> repository) : base(unitWork, repository)
        {
        }
    }
}
