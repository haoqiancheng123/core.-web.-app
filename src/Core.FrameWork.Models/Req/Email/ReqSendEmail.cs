﻿using Core.FrameWork.Commons.Helper.Validate.Attribute;
using Core.FrameWork.Commons.Hepler.Validate;
using Core.FrameWork.Commons.Hepler.Validate.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Req.Email
{
    /// <summary>
    /// 发送邮件提交表单
    /// </summary>
    public class ReqSendEmail : ReqBaseModel
    {
        /// <summary>
        /// 收件人
        /// </summary>
        [ListNotNull]
        public List<string> Recipients { get; set; }
        /// <summary>
        /// 抄送人
        /// </summary>
        [ListNotNull]
        public List<string> Cc { get; set; } = new List<string>();
        /// <summary>
        /// 密抄人
        /// </summary>
        [ListNotNull]
        public List<string> Bcc { get; set; } = new List<string>();
        /// <summary>
        /// 邮件正文
        /// </summary>
        [Required]
        public string Body { get; set; } = string.Empty;
        /// <summary>
        /// 主题
        /// </summary>
        [Required]
        public string Subject { get; set; } = string.Empty;
    }
}
