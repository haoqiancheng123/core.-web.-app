﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models.System
{
    /// <summary>
    /// 系统验证码数据库
    /// </summary>
    [Table("SysRecord")]
    public class SysRecord : BaseEntity
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int TypeId { get; set; } = 0;
        /// <summary>
        /// 接收方
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Receiver { get; set; } = string.Empty;
        /// <summary>
        /// 验证码
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Code { get; set; } = string.Empty;
        /// <summary>
        /// 发送内容
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Content { get; set; } = string.Empty;
        /// <summary>
        /// 状态
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int State { get; set; } = 0;
    }
}
