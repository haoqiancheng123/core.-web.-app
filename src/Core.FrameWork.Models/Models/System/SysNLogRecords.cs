﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models.System
{
    /// <summary>
    /// 记录NLog日志
    /// </summary>
    [Table("SysNLogRecords")]
    public class SysNLogRecords : BaseEntity
    {
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime LogDate { get; set; }
        /// <summary>
        /// 级别
        /// </summary>
        public string LogLevel { get; set; }
        /// <summary>
        /// 事件日志上下文
        /// </summary>
        public string LogType { get; set; }
        /// <summary>
        /// 事件标题
        /// </summary>
        public string LogTitle { get; set; }
        /// <summary>
        /// 记录器名字
        /// </summary>
        public string Logger { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 异常信息
        /// </summary>
        public string Exception { get; set; }
        /// <summary>
        /// 机器名称
        /// </summary>
        public string MachineName { get; set; }
        /// <summary>
        /// ip
        /// </summary>
        public string MachineIp { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string NetRequestMethod { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string NetRequestUrl { get; set; }
        /// <summary>
        /// 是否授权
        /// </summary>
        public string NetUserIsauthenticated { get; set; }
        /// <summary>
        /// 授权类型
        /// </summary>
        public string NetUserAuthtype { get; set; }
        /// <summary>
        /// 身份认证
        /// </summary>
        public string NetUserIdentity { get; set; }
    }
}
