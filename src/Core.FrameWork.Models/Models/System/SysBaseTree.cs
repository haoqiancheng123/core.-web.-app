﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models.System
{
    /// <summary>
    /// BreeTree 树配置
    /// </summary>
    [Table("SysBaseTree")]
    public class SysBaseTree : BaseEntity, IDeleteAudited
    {
        /// <summary>
        /// 树名称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string TreeName { get; set; } = string.Empty;
        /// <summary>
        /// 上级Id
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid PId { get; set; } = Guid.Empty;
        /// <summary>
        /// 字段1
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend1 { get; set; } = string.Empty;
        /// <summary>
        /// 字段2
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend2 { get; set; } = string.Empty;
        /// <summary>
        /// 字段3
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend3 { get; set; } = string.Empty;
        /// <summary>
        /// 字段4
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend4 { get; set; } = string.Empty;
        /// <summary>
        /// 字段5
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend5 { get; set; } = string.Empty;
        /// <summary>
        /// 字段6
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Extend6 { get; set; } = string.Empty;
        /// <summary>
        /// 图片1
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ImgUrl1 { get; set; } = string.Empty;
        /// <summary>
        /// 图片2
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ImgUrl2 { get; set; } = string.Empty;
        /// <summary>
        /// 图片3
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ImgUrl3 { get; set; } = string.Empty;
        /// <summary>
        /// 富文本
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Context { get; set; } = string.Empty;
        /// <summary>
        /// 排序
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int Sort { get; set; } = 0;

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public bool State { get; set; } = false;
        /// <summary>
        /// 开始时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime StartTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 结束时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime EndTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 路径
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Path { get; set; } = string.Empty;
        /// <summary>
        /// 是否删除
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IsDelete { get; set; } = 0;
        /// <summary>
        /// 删除时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime DeleteTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 删除用户
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid DeleteUser { get; set; } = Guid.Empty;
        /// <summary>
        /// 删除用户名称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteUserName { get; set; } = string.Empty;
    }
}
