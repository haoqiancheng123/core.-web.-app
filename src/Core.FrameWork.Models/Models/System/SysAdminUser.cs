﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models.System
{
    /// <summary>
    /// 后台管理用户
    /// </summary>
    [Table("SysAdminUser")]
    public class SysAdminUser : BaseEntity, IDeleteAudited
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string Account { get; set; } = string.Empty;
        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string Email { get; set; } = string.Empty;
        /// <summary>
        /// 电话
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string PhoneNumber { get; set; } = string.Empty;
        /// <summary>
        /// 密码
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string Password { get; set; } = string.Empty;
        /// <summary>
        /// 头像
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Avatar { get; set; } = string.Empty;
        /// <summary>
        /// 真实姓名
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string TrueName { get; set; } = string.Empty;
        /// <summary>
        /// 角色
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Role { get; set; } = string.Empty;
        /// <summary>
        /// 地址
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string Address { get; set; } = string.Empty;
        /// <summary>
        /// 公司
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]

        public string Company { get; set; } = string.Empty;
        /// <summary>
        /// 是否删除
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IsDelete { get; set; } = 0;
        /// <summary>
        /// 删除时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime DeleteTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 删除用户
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid DeleteUser { get; set; } = Guid.Empty;
        /// <summary>
        /// 删除用户名称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteUserName { get; set; } = string.Empty;
    }
}
