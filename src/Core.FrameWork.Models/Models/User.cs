﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models
{
    /// <summary>
    /// 用户基类
    /// </summary>
    [Table("User")]
    public class User: BaseEntity, IDeleteAudited
    {
        /// <summary>
        /// 账户
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string RealName { get; set; }

        /// <summary>
        /// 呢称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string HeadIcon { get; set; }

        /// <summary>
        /// 性别,1=男，0=未知，2=女
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual int Gender { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string MobilePhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string Email { get; set; }

        /// <summary>
        /// 国家
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string Country { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string Province { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string City { get; set; }

        /// <summary>
        /// 地区
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string District { get; set; }

        /// <summary>
        /// 是否管理员
        /// </summary>
        public virtual bool? IsAdministrator { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IsDelete { get; set; } = 0;
        /// <summary>
        /// 删除时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime DeleteTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 删除用户
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid DeleteUser { get; set; } = Guid.Empty;
        /// <summary>
        /// 删除用户名称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteUserName { get; set; } = string.Empty;
    }
}
