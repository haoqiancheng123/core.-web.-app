﻿using Core.FrameWork.Commons.ORM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Models
{
    /// <summary>
    /// 测试
    /// </summary>
    [Table("Test")]
    public class Test : BaseEntity, IDeleteAudited
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 是否删除
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IsDelete { get; set; } = 0;
        /// <summary>
        /// 删除时间
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public DateTime DeleteTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 删除用户
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid DeleteUser { get; set; } = Guid.Empty;
        /// <summary>
        /// 删除用户名称
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteUserName { get; set; } = string.Empty;
    }
}
