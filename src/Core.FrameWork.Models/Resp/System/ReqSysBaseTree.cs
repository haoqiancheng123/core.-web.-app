﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Resp.System
{
    public class ReqSysBaseTree
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 树名称
        /// </summary>
        public string TreeName { get; set; }
        /// <summary>
        /// 上级Id
        /// </summary>
        public Guid PId { get; set; }
        /// <summary>
        /// 字段1
        /// </summary>
        public string Extend1 { get; set; }
        /// <summary>
        /// 字段2
        /// </summary>
        public string Extend2 { get; set; }
        /// <summary>
        /// 字段3
        /// </summary>
        public string Extend3 { get; set; }
        /// <summary>
        /// 字段4
        /// </summary>
        public string Extend4 { get; set; }
        /// <summary>
        /// 字段5
        /// </summary>
        public string Extend5 { get; set; }
        /// <summary>
        /// 字段6
        /// </summary>
        public string Extend6 { get; set; }
        /// <summary>
        /// 图片1
        /// </summary>
        public string ImgUrl1 { get; set; } 
        /// <summary>
        /// 图片2
        /// </summary>
        public string ImgUrl2 { get; set; }
        /// <summary>
        /// 图片3
        /// </summary>
        public string ImgUrl3 { get; set; } 
        /// <summary>
        /// 富文本
        /// </summary>
        public string Context { get; set; } 
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; } 
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; } 
        /// <summary>
        /// 层数
        /// </summary>
        public string Cnt { get; set; }
        /// <summary>
        /// 上级TreeName
        /// </summary>

        public string PName { get; set; }
    }
    public class ReqSysBaseTreeZtree
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
