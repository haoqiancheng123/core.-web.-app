﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Resp.System
{
    public class RespAdminUser
    {
        public Guid Id { get; set; }

        public string TrueName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public List<Guid> RoleId { get; set; }

        public List<string> RoleName { get; set; }

        public bool Enabled { get; set; }
    }
}
