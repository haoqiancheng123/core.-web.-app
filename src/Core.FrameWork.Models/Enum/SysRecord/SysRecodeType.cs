﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models.Enum.SysRecord
{
    public enum SysRecodeType
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        Email=0,
        /// <summary>
        /// 短信
        /// </summary>
        Sms
    }
}
