﻿using Core.FrameWork.Commons;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models
{
    /// <summary>
    /// 接口配置
    /// </summary>
    public class APIConfig : Configs
    {
        static APIConfig()
        {

            ApiSetting = new ApiSetting();
            HttpClientPolly = new HttpClientPolly();
            JwtOption = new JwtOption();
        }
        public static ApiSetting ApiSetting { get; set; }
        public static JwtOption JwtOption { get; set; }
        public static HttpClientPolly HttpClientPolly { get; set; }
    }
    /// <summary>
    /// API全局配置
    /// </summary>
    public class ApiSetting:AppSetting
    {
        /// <summary>
        /// 是否使用Swagger
        /// </summary>
        public bool UseSwagger => AppCenter.Configuration.GetSection("AppSetting")["UseSwagger"].ToBool();
        /// <summary>
        /// BreeTree缓存过期时间
        /// </summary>
        public int BaseTreeCacheExpiration => AppCenter.Configuration.GetSection("AppSetting")["BaseTreeCacheExpiration"].ToInt();
        /// <summary>
        /// 是否开始跨域拦截
        /// </summary>
        public bool CorsEnableAllIPs => AppCenter.Configuration.GetSection("AppSetting")["CorsEnableAllIPs"].ToBool();
        /// <summary>
        /// 跨域策略名称
        /// </summary>
        public string CorsPolicyName => AppCenter.Configuration.GetSection("AppSetting")["CorsPolicyName"];

        public string CorsIPs => AppCenter.Configuration.GetSection("AppSetting")["CorsIPs"];
        /// <summary>
        /// 是否开启记录IP
        /// </summary>
        public bool IPLog => AppCenter.Configuration.GetSection("AppSetting")["IPLog"].ToBool();
        /// <summary>
        /// 是否记录用户行为日志
        /// </summary>
        public bool RecordUsersLogs => AppCenter.Configuration.GetSection("AppSetting")["RecordUsersLogs"].ToBool();
    }
    #region JWT配置 ------------------------
    /// <summary>
    /// JsonWebToken配置模型。
    /// </summary>
    public class JwtOption
    {
        public string Issuer => AppCenter.Configuration.GetSection("JwtOption")["Issuer"];
        public string Audience => AppCenter.Configuration.GetSection("JwtOption")["Audience"];
        public string Secret => AppCenter.Configuration.GetSection("JwtOption")["Secret"];
        public int Expiration => AppCenter.Configuration.GetSection("JwtOption")["Expiration"].ToInt();

        public int refreshJwtTime => AppCenter.Configuration.GetSection("JwtOption")["refreshJwtTime"].ToInt();
    }
    #endregion
    #region HttpClientPolly配置
    public class HttpClientPolly
    {
        /// <summary>
        /// HttpClient域名称
        /// </summary>
        public string HttpClientName => AppCenter.Configuration.GetSection("HttpClientPolly")["HttpClientName"];
        /// <summary>
        /// 重试-次数
        /// </summary>
        public int RetryNum => AppCenter.Configuration.GetSection("HttpClientPolly")["RetryNum"].ToInt();
        /// <summary>
        /// 断路器-错误次数配置
        /// </summary>
        public int HandledEventsAllowedBeforeBreaking => AppCenter.Configuration.GetSection("HttpClientPolly")["HandledEventsAllowedBeforeBreaking"].ToInt();
        /// <summary>
        /// 断路器-断开时间（秒）
        /// </summary>
        public int DurationOfBreak => AppCenter.Configuration.GetSection("HttpClientPolly")["DurationOfBreak"].ToInt();
        /// <summary>
        /// 超时时间
        /// </summary>
        public int Timeout => AppCenter.Configuration.GetSection("HttpClientPolly")["Timeout"].ToInt();
    }
    #endregion
}
