﻿using Core.FrameWork.Commons;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Models
{
    /// <summary>
    /// 后台管理配置
    /// </summary>
    public class AdminConfig : Configs
    {
        static AdminConfig()
        {
            AdminSetting = new AdminSetting();

        }
        public static AdminSetting AdminSetting { get; set; }
    }
    /// <summary>
    /// API全局配置
    /// </summary>
    public class AdminSetting:AppSetting
    {
        /// <summary>
        /// 万能验证码
        /// </summary>
        public string OverCode => AppCenter.Configuration.GetSection("AppSetting")["OverCode"];

        public string DownloadUrl => AppCenter.Configuration.GetSection("AppSetting")["DownloadUrl"];

        public bool IsBlob => AppCenter.Configuration.GetSection("AppSetting")["IsBlob"].ToBool();

        public string Role => AppCenter.Configuration.GetSection("AppSetting")["Role"];
        
    }
}
