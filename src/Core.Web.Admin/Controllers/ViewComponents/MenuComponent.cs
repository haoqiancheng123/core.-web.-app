﻿using Core.Web.Admin.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Core.Web.Admin.Controllers.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var currentUser = HttpContext.GetCurrentUser();
            return View("Menu", currentUser);
        }
        
    }
}
