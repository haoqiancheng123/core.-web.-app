﻿using Microsoft.AspNetCore.Mvc;
using Core.Web.Admin.Extensions;

namespace Core.Web.Admin.Controllers.Controllers
{
    public class HeaderViewComponent: ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var currentUser = HttpContext.GetCurrentUser();
            return View("Header", currentUser);
        }
        
    }
}
