﻿using Core.FrameWork.Commons.ORM.Abstraction;
using Core.Web.Admin.Dto;
using Core.Web.Admin.Extensions;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Admin.Controllers
{
    [Route("[controller]/[action]")]
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class BaseController : Controller
    {
        protected readonly IDbContextCore _dbContextCore;
        public BaseController(IDbContextCore dbContextCore)
        {
            _dbContextCore = dbContextCore;
        }
        public CurrentUserInfo CurrentUser
        {
            get
            {
                return HttpContext.GetCurrentUser();
            }
        }
    }
}
