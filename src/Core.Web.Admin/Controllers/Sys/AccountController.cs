﻿//using ARC.Common.Help;
//using Core.FrameWork.Commons;
//using Core.FrameWork.Commons.ApiResult;
//using Core.FrameWork.Commons.Extensions;
//using Core.FrameWork.Commons.ORM.Abstraction;
//using Core.FrameWork.Models.Req.AdminUser;
//using Core.Web.Admin.Utils;
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.IdentityModel.Tokens;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Claims;
//using System.Text;
//using System.Threading.Tasks;

//namespace Core.Web.Admin.Controllers.Sys
//{
//    public class AccountController : BaseController
//    {
//        private readonly IAdminUserService _adminUserService;
//        public AccountController(IDbContextCore dbContextCore, IAdminUserService adminUserService) : base(dbContextCore)
//        {
//            _adminUserService = adminUserService;
//        }
//        [AllowAnonymous]
//        public IActionResult Login(string ReturnUrl = "")
//        {
//            ViewBag.ReturnUrl = ReturnUrl;
//            return View();
//        }
//        [AllowAnonymous]
//        [HttpPost]
//        public async Task<CommonResult> SignLogin([FromForm] ReqAdminUserSignLogin reqAdminUserSignLogin)
//        {
//            var reqResult = reqAdminUserSignLogin.Validate();
//            if (!reqResult.Status)
//                return ApiRespHelp.getError("-100", reqResult.Message);

//            var adminUser = await _adminUserService.GetAdminUser(reqAdminUserSignLogin);

//            if (adminUser == null)
//                return ApiRespHelp.getError("-100", "账号不存在!");

//            if (!adminUser.Enabled)
//                return ApiRespHelp.getError("-100", "账号已被禁用!");

//            if (!adminUser.Password.Equals(reqAdminUserSignLogin.Password))
//                return ApiRespHelp.getError("-100", "密码错误!");

//            TokenProvider TokenProvider = new TokenProvider();
//            var claimsIdentity = TokenProvider.GetClaimsIdentity(adminUser, "Admin");//生成Claims

//            var key = Encoding.UTF8.GetBytes(Configs.JwtOption.Secret);
//            var authTime = DateTime.UtcNow;//授权时间
//            var expires = authTime.Add(TimeSpan.FromMinutes(Configs.JwtOption.Expiration));//过期时间

//            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
//            HttpContext.SignInAsync(claimsPrincipal, new AuthenticationProperties
//            {
//                IsPersistent = true,
//                ExpiresUtc = expires,
//            }).Wait();

//            string returnUrl = "~/Home/Index";
//            if (!string.IsNullOrEmpty(reqAdminUserSignLogin.ReturnUrl))
//                returnUrl = reqAdminUserSignLogin.ReturnUrl;

//            return ApiRespHelp.getSuccess(Url.Content(returnUrl));
//        }
//    }
//}
