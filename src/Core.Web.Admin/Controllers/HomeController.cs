﻿using Core.FrameWork.Commons.ORM.Abstraction;
using Microsoft.AspNetCore.Mvc;

namespace Core.Web.Admin.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IDbContextCore dbContextCore) : base(dbContextCore)
        {

        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
