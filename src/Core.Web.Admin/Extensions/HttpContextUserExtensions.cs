﻿using Core.Web.Admin.Dto;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Web.Admin.Extensions
{
    public static class HttpContextUserExtensions
    {
        public static CurrentUserInfo GetCurrentUser(this HttpContext httpContext)
        {
            CurrentUserInfo currentUser = new CurrentUserInfo();
            if (httpContext.User.Claims.Count() > 0)
            {
                var roles = httpContext.User.Claims.Where(m => m.Type == ClaimTypes.Role).Select(m => m.Value).ToList();
                currentUser.Id = httpContext.User.Claims.FirstOrDefault(m => m.Type == JwtClaimTypes.Id)?.Value;
                currentUser.TrueName = httpContext.User.Identity.Name;
                currentUser.Roles = roles;
                currentUser.Email = httpContext.User.Claims.FirstOrDefault(m => m.Type == JwtClaimTypes.Email)?.Value;
            }
            return currentUser;
        }
    }
}
